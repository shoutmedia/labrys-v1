    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="logo-container footer-logo">
                    <a class="logo" href="<?php echo home_url(); ?>">
                        <?php echo labrys_get_logo('white'); ?>
                    </a>
                </div>
                <div class="col col-navigation">
                    <div class="footer-content">
                        <nav class="footer-nav">
                        <?php
                            if ( has_nav_menu( 'footer' ) ) {
                                wp_nav_menu(
                                    array(
                                        'theme_location'    => 'footer',
                                        'container_class'   => 'footer-menu-container',
                                        'menu_class'        => 'menu',
                                    )
                                );
                            }
                        ?>
                        </nav>
                    </div>
                </div>
                <div class="col col-contact">
                    <div class="footer-content">
                        <?php echo labrys_get_contact(); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col col-copyright">
                    <p>&copy; <?php
                        $copyright_name = get_field( 'copyright_name', 'option' );
                        $copyright_name = ( $copyright_name ) ? $copyright_name : get_bloginfo('name');
                        echo date('Y') .' '. $copyright_name;
                    ?></p>
                </div>
                <?php
                    /*
                        <div class="col col-social">
                            <?php echo labrys_get_social_links(); ?>
                        </div>
                    */
                ?>
                <div class="col col-credit">
                    <?php echo labrys_get_credit(); ?>
                </div>
            </div>
        </div>
    </footer>

</div>

<?php wp_footer(); ?>

<style media="screen">
    body {
        opacity: 1;
    }
</style>

</body>
</html>
