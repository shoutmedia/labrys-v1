wp.domReady( () => {

////// Block Styles

    // core/heading

    wp.blocks.registerBlockStyle( 'core/heading', [
        {
		    name: 'default',
		    label: 'Default',
		    isDefault: true,
        },
        {
            name: 'arrow',
            label: 'Arrow',
        }
    ]);

    // core/paragraph

    wp.blocks.registerBlockStyle( 'core/paragraph', [
        {
		    name: 'default',
		    label: 'Default',
		    isDefault: true,
        },
        {
            name: 'indent',
            label: 'Indent',
        }
    ]);

    // core/button

    wp.blocks.registerBlockStyle( 'core/button', [
        {
		    name: 'default',
		    label: 'Default',
		    isDefault: true,
        },
        {
            name: 'arrow',
            label: 'Arrow',
        }
    ]);

    // core/list

	wp.blocks.registerBlockStyle( 'core/list', [
        {
		    name: 'default',
		    label: 'Default',
		    isDefault: true,
        },
        {
            name: 'word',
            label: 'Word Files',
        },
        {
            name: 'pdf',
            label: 'PDF Files',
        },
        {
            name: 'website',
            label: 'Website Links',
        },
        {
            name: 'arrow',
            label: 'Arrows',
        }
    ]);

////// Rich Text Toolbar

    // formats

    var formats = [,
		{
            title: 'No Wrap',
            className: 'text-nowrap',
            tagName: 'span',
            icon: 'minus'
		},
        {
            title: 'Text Size Small',
            className: 'text-sm',
            tagName: 'span',
            icon: 'arrow-down'
        },
		{
            title: 'Text Size Large',
            className: 'text-lg',
            tagName: 'span',
            icon: 'arrow-up'
        },
		{
            title: 'Text Size Extra Large',
            className: 'text-xl',
            tagName: 'span',
            icon: 'arrow-up'
        }
    ];

    formats.forEach( function(format) {
        wp.richText.registerFormatType(
            'labrys/'+ format.className, {
                title: format.title,
                tagName: format.tagName,
                className: format.className,
                edit: function( props ) {
                    return wp.element.createElement(
                        wp.blockEditor.RichTextToolbarButton,
                        {
                            icon: format.icon,
                            title: format.title,
                            onClick: function() {
                                props.onChange(
                                    wp.richText.toggleFormat(props.value, {
                                        type: 'labrys/'+ format.className
                                    })
                                );
                            },
                            isActive: props.isActive,
                        }
                    );
                }
            }
        );
    });

} );
