<?php

function labrys_register_projects_post_type() {
    // projects
    $args = array(
        'labels'            => labrys_get_post_type_labels( 'Project', 'Projects' ),
        'rewrite'           => array( 'slug' => 'project' ),
        'supports'          => array( 'title', 'thumbnail' ),
        'hierarchical'      => false,
        'public'            => true,
        'show_ui'           => true,
        'show_in_rest'      => true,
        'menu_icon'         => 'dashicons-hammer',
        'menu_position'     => 20
    );
    register_post_type( 'labrys-projects', $args );
}
add_action( 'init', 'labrys_register_projects_post_type' );

// admin columns

function labrys_projects_columns_type( $columns ) {
    $columns = array(
        'cb' => $columns['cb'],
        'title' => __( 'Title', DOMAIN ),
        'type' => __( 'Project Complete', DOMAIN ),
        'date' => __( 'Date', DOMAIN ),
    );
    return $columns;
}
add_filter( 'manage_labrys-projects_posts_columns', 'labrys_projects_columns_type' );

function labrys_projects_custom_column_type( $column, $post_id ) {
    switch ( $column ) {
        case 'type':
            $project_complete = get_field( 'project_complete', $post_id );
            echo ( $project_complete ? 'Yes' : 'No' );
        break;
    }
}
add_action( 'manage_labrys-projects_posts_custom_column', 'labrys_projects_custom_column_type', 10, 2);

// ajax

function labrys_get_project( $project_id ) {
    // initialize return array
    $return_array = array('id' => 0, 'content' => '');
    // query projects
    $args = array(
        'post_type' => 'labrys-projects',
        'nopaging' => true,
        'p' => $project_id
    );
    $query = new WP_Query($args);
    if ( $query->found_posts ) {
        $projects = $query->get_posts();
        $project = $projects[0];
        $str = '<div class="modal-project">';
            $str .= '<div class="container">';
                $str .= '<div class="row">';
                    $str .= '<div class="col-lg-12 order-lg-2 d-lg-flex align-items-lg-center">';
                        $thumbnail = get_the_post_thumbnail( $project->ID );
                        $str .= '<p>'. $thumbnail .'</p>';
                    $str .= '</div>';
                    $str .= '<div class="col-lg-12 order-lg-1 d-lg-flex align-items-lg-center">';
                        $str .= '<div class="project-content">';
                            $str .= '<h2>'. get_the_title( $project->ID ) .'</h2>';
                            $city = get_field('city', $project->ID);
                            if ($city) {
                                $str .= '<p class="project-city"><strong>'. $city .'</strong></p>';
                            }
                            $content = get_field('content', $project->ID);
                            $str .= $content;
                            $str .= '<p class="mt-4"><a href="#close" class="project-modal-close">Close</a></p>';
                        $str .= '</div>';
                    $str .= '</div>';
                $str .= '</div>';
            $str .= '</div>';
        $str .= '</div>';
        $return_array = array(
            'id'        => $project->ID,
            'content'   => $str
        );
    }
    return $return_array;
}

// labrys_ajax_get_project

function labrys_ajax_get_project() {
    if ( wp_doing_ajax() ) { // ajax request
        // security check
        check_ajax_referer( labrys_ajax_key(), 'key' );
        // labrys_get_project
        $project_id = intval( $_POST['project'] );
        $return_array = labrys_get_project( $project_id );
        // return data
        wp_send_json($return_array);
    } // end ajax request
    wp_die();
}
add_action( 'wp_ajax_nopriv_get_project', 'labrys_ajax_get_project' );
add_action( 'wp_ajax_get_project', 'labrys_ajax_get_project' );