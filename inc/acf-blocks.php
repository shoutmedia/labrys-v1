<?php

// block category

function labrys_block_category( $categories, $post ) {
	$theme_category = array(
		array(
			'slug' => DOMAIN,
			'title' => __( 'Labrys', DOMAIN ),
		),
	);
	return array_merge( $theme_category, $categories );
}
add_filter( 'block_categories', 'labrys_block_category', 10, 2);

// render callback

function labrys_acf_block_render_callback( $block ) {
	$slug = str_replace('acf/', '', $block['name']);
	$filename = get_theme_file_path( '/inc/blocks/'. $slug .'.php' );
	if ( file_exists( $filename ) ) {
		include( $filename );
	}
}

// register acf blocks

function labrys_acf_register_blocks() {
	if ( function_exists('acf_register_block') ) {
		$acf_blocks = labrys_acf_blocks();
		foreach( $acf_blocks as $acf_block ) {
			$acf_block['category'] = DOMAIN;
			$acf_block['render_callback'] = 'labrys_acf_block_render_callback';
			acf_register_block( $acf_block );
		}
	}
}
add_action('acf/init', 'labrys_acf_register_blocks');

// acf blocks

function labrys_acf_blocks() {

	$acf_blocks = array();

	// acf/section
	$acf_blocks[] = array(
		'name'				=> 'section',
		'title'				=> __('Section', DOMAIN),
		'description'		=> __('A block to display a section container.', DOMAIN),
		'icon'				=> 'align-wide',
		'mode'				=> 'preview',
		'align'				=> 'wide',
		'supports'			=> array( 'mode' => true, 'align' => array( 'full', 'wide' ), 'jsx' => true ),
		'keywords'			=> array( 'section', 'container' )
	);

	// acf/banner
	$acf_blocks[] = array(
		'name'				=> 'banner',
		'title'				=> __('Banner', DOMAIN),
		'description'		=> __('A block to display a banner.', DOMAIN),
		'icon'				=> 'format-image',
		'mode'				=> 'edit',
		'align'				=> 'full',
		'supports'			=> array( 'mode' => true, 'align' => array( 'full' ), 'jsx' => true ),
		'keywords'			=> array( 'banner', 'header' )
	);

	// acf/cards
	$acf_blocks[] = array(
		'name'				=> 'cards',
		'title'				=> __('Cards', DOMAIN),
		'description'		=> __('A block to display a group of card widgets.', DOMAIN),
		'icon'				=> 'slides',
		'mode'				=> 'edit',
		'align'				=> 'full',
		'supports'			=> array( 'mode' => true, 'align' => array( 'full' ) ),
		'keywords'			=> array( 'card', 'widget', 'link' )
	);

	// acf/grid
	$acf_blocks[] = array(
		'name'				=> 'grid',
		'title'				=> __('Grid', DOMAIN),
		'description'		=> __('A block to display a grid container.', DOMAIN),
		'icon'				=> 'grid-view',
		'mode'				=> 'preview',
		'align'				=> 'full',
		'supports'			=> array( 'mode' => true, 'align' => array( 'full' ), 'jsx' => true ),
		'keywords'			=> array( 'grid', 'container' )
	);

	// acf/grid-item
	$acf_blocks[] = array(
		'name'				=> 'grid-item',
		'title'				=> __('Grid Item', DOMAIN),
		'description'		=> __('A block to display content within a grid.', DOMAIN),
		'icon'				=> 'format-aside',
		'mode'				=> 'preview',
		'supports'			=> array( 'mode' => true, 'align' => false, 'jsx' => true ),
		'keywords'			=> array( 'grid', 'item', 'content' )
	);

	// acf/flex-container
	$acf_blocks[] = array(
		'name'				=> 'flex-container',
		'title'				=> __('Flex Container', DOMAIN),
		'description'		=> __('A block to display a flex container.', DOMAIN),
		'icon'				=> 'editor-table',
		'mode'				=> 'preview',
		'align'				=> '',
		'supports'			=> array( 'mode' => true, 'align' => array( 'full', 'wide' ), 'jsx' => true ),
		'keywords'			=> array( 'flex', 'column', 'container' )
	);

	// acf/flex-column
	$acf_blocks[] = array(
		'name'				=> 'flex-column',
		'title'				=> __('Flex Column', DOMAIN),
		'description'		=> __('A block to display a column within a flex container.', DOMAIN),
		'icon'				=> 'columns',
		'mode'				=> 'preview',
		'supports'			=> array( 'mode' => true, 'align' => false, 'jsx' => true ),
		'keywords'			=> array( 'flex', 'column' )
	);

	// acf/map
	$acf_blocks[] = array(
		'name'				=> 'map',
		'title'				=> __('Map', DOMAIN),
		'description'		=> __('A block to display a map with locations.', DOMAIN),
		'icon'				=> 'location',
		'mode'				=> 'edit',
		'align'				=> 'full',
		'supports'			=> array( 'mode' => true, 'align' => array( 'full' ) ),
		'keywords'			=> array( 'map', 'marker', 'location' )
	);

	// acf/post-feed
	$acf_blocks[] = array(
		'name'				=> 'post-feed',
		'title'				=> __('Post Feed', DOMAIN),
		'description'		=> __('A block to display a feed of posts.', DOMAIN),
		'icon'				=> 'feedback',
		'mode'				=> 'edit',
		'supports'			=> array( 'mode' => true, 'align' => array( 'full' ) ),
		'keywords'			=> array( 'post', 'feed' )
	);

	// acf/slides
	$acf_blocks[] = array(
		'name'				=> 'slides',
		'title'				=> __('Slides', DOMAIN),
		'description'		=> __('A block to display slide content.', DOMAIN),
		'icon'				=> 'format-gallery',
		'mode'				=> 'edit',
		'align'				=> 'full',
		'supports'			=> array( 'mode' => true, 'align' => array( 'full' ) ),
		'keywords'			=> array( 'slide', 'banner' )
	);

	// acf/tabs
	$acf_blocks[] = array(
		'name'				=> 'tabs',
		'title'				=> __('Tabs', DOMAIN),
		'description'		=> __('A block to display tabbed content.', DOMAIN),
		'icon'				=> 'index-card',
		'mode'				=> 'edit',
		'supports'			=> array( 'mode' => true, 'align' => false ),
		'keywords'			=> array( 'tab' )
	);

	// acf/icon
	$acf_blocks[] = array(
		'name'				=> 'icon',
		'title'				=> __('Icon', DOMAIN),
		'description'		=> __('A block to display an icon.', DOMAIN),
		'icon'				=> 'share',
		'mode'				=> 'edit',
		'supports'			=> array( 'mode' => true, 'align' => false ),
		'keywords'			=> array( 'icon' )
	);

	// acf/contact
	$acf_blocks[] = array(
		'name'				=> 'contact',
		'title'				=> __('Contact', DOMAIN),
		'description'		=> __('A block to display contact information.', DOMAIN),
		'icon'				=> 'phone',
		'mode'				=> 'edit',
		'supports'			=> array( 'mode' => true, 'align' => false ),
		'keywords'			=> array( 'contact', 'info' )
	);

	return $acf_blocks;

}