<?php

/*
    // Gutenberg block templates
*/

// temporarily display block info

function labrys_display_post_blocks() {
	global $post;
	print_r( esc_html( $post->post_content ) );
}
// add_action( 'wp_footer', 'labrys_display_post_blocks' );

// block template for pages

function labrys_page_block_template() {
	$post_type_object = get_post_type_object( 'page' );
	$post_type_object->template = array(
		array( 'acf/banner' ),
	);
	// $post_type_object->template_lock = 'all';
}
// add_action( 'init', 'labrys_page_block_template' );