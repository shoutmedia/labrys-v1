<?php

// google analytics

function labrys_google_analytics() {
    $tracking_id = get_field( 'google_analytics_tracking_id', 'option' );
    if ( $tracking_id ) {
        $str = '';
        $str .= '<!-- Global site tag (gtag.js) - Google Analytics -->'."\n";
        $str .= "\t".'<script async src="https://www.googletagmanager.com/gtag/js?id='. $tracking_id .'"></script>'."\n";
        $str .= "\t".'<script>'."\n";
        $str .= "\t\t".'window.dataLayer = window.dataLayer || [];'."\n";
        $str .= "\t\t".'function gtag(){dataLayer.push(arguments);}'."\n";
        $str .= "\t\t".'gtag(\'js\', new Date());'."\n";
        $str .= "\t\t".'gtag(\'config\', \''. $tracking_id .'\');'."\n";
        $str .= "\t".'</script>';
        echo $str;
    }
}
add_action('wp_head', 'labrys_google_analytics', 1);

// facebook app_id

function labrys_fb_app_id() {
    $app_id = 966242223397117; // default app id
    $tag = '<meta property="fb:app_id" content="%d" />';
    echo sprintf($tag, $app_id);
}
add_action( 'wp_head', 'labrys_fb_app_id' );

// header_tracking_codes

function labrys_header_tracking_codes() {
    $header_tracking_codes = get_field( 'header_tracking_codes', 'option' );
    if ( $header_tracking_codes ) {
        echo $header_tracking_codes;
    }
}
add_action('wp_head', 'labrys_header_tracking_codes', 5);

// footer_tracking_codes

function labrys_footer_tracking_codes() {
    $footer_tracking_codes = get_field( 'footer_tracking_codes', 'option' );
    if ( $footer_tracking_codes ) {
        echo $footer_tracking_codes;
    }
}
add_action('wp_footer', 'labrys_footer_tracking_codes', 30);

?>
