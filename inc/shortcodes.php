<?php

// [display-alert content="Alert"]

function labrys_shortcode_display_alert( $atts ) {
	$a = shortcode_atts( array(
        'content' => 'Alert',
    ), $atts );
	$str = '';
	if ( $a['content'] ) {
		$str .= '<div class="alert">'. $a['content'] .'</div>';
	}
	return $str;
}
add_shortcode( 'display-alert', 'labrys_shortcode_display_alert' );
