<?php

// labrys_get_post_type_labels

function labrys_get_post_type_labels( $singular, $plural ) {
    $domain = DOMAIN;
    return array(
        'name'                      => __( $plural, $domain ),
        'singular_name'             => __( $singular, $domain ),
        'add_new_item'              => __( 'Add New '. $singular, $domain ),
        'edit_item'                 => __( 'Edit '. $singular, $domain ),
        'new_item'                  => __( 'New '. $singular, $domain ),
        'view_item'                 => __( 'View '. $singular, $domain ),
        'view_items'                => __( 'View '. $plural, $domain ),
        'search_items'              => __( 'Search '. $plural, $domain ),
        'all_items'                 => __( 'All '. $plural, $domain ),
        'archives'                  => __( $singular .' Archives', $domain ),
        'attributes'                => __( $singular .' Attributes', $domain ),
        'items_list_navigation'     => __( $plural .' list navigation', $domain ),
        'items_list'                => __( $plural .' list', $domain ),
        'item_published'            => __( $singular .' published.', $domain ),
        'item_published_privately'  => __( $singular .' published privately.', $domain ),
        'item_reverted_to_draft'    => __( $singular .' reverted to draft.', $domain ),
        'item_scheduled'            => __( $singular .' scheduled.', $domain ),
        'item_updated'              => __( $singular .' updated.', $domain ),
    );
}

// labrys_get_taxonomy_labels

function labrys_get_taxonomy_labels( $singular, $plural ) {
    $domain = DOMAIN;
    return array(
        'name' => __( $plural, $domain ),
        'singular_name' => __( $singular, $domain ),
        'search_items' =>  __( 'Search '. $plural, $domain ),
        'all_items' => __( 'All '. $plural, $domain ),
        'parent_item' => __( 'Parent '. $singular, $domain ),
        'parent_item_colon' => __( 'Parent '. $singular .':', $domain ),
        'edit_item' => __( 'Edit '. $singular, $domain ),
        'update_item' => __( 'Update '. $singular, $domain ),
        'add_new_item' => __( 'Add New '. $singular, $domain ),
        'new_item_name' => __( 'New '. $singular .' Name', $domain ),
        'back_to_items' => __( 'Back to '. $plural, $domain ),
        'menu_name' => __( $plural, $domain ),
    );
}

// labrys_get_thumbnail

function labrys_get_thumbnail( $thumbnail_id = false, $size = 'thumbnail' ) {
    $thumbnail_array = array();
    if ( $thumbnail_id ) {
        $thumbnail = wp_get_attachment_image_src( $thumbnail_id, $size );
        if ($thumbnail) {
            $thumbnail_alt = get_post_meta( $thumbnail_id, '_wp_attachment_image_alt', true );
            $thumbnail_caption = wp_get_attachment_caption( $thumbnail_id );
            $thumbnail_array = array(
                'id' => $thumbnail_id,
                'src' => $thumbnail[0],
                'width' => $thumbnail[1],
                'height' => $thumbnail[2],
                'is_intermediate' => $thumbnail[3],
                'alt' => $thumbnail_alt,
                'caption' => ''
            );
        }
    }
    return $thumbnail_array;
}

// labrys_get_logo

function labrys_get_logo( $class = '', $id = '' ) {
    $custom_logo_id = get_theme_mod( 'custom_logo' );
    if ($custom_logo_id) {
        $custom_logo_url = wp_get_attachment_image_url( $custom_logo_id, 'full' );
        return '<img src="'. esc_url( $custom_logo_url ) .'" alt="'. get_bloginfo('name') .'">';
    } else {
        return labrys_svg_logo( $class, $id );
    }
}

// labrys_get_description

function labrys_get_description() {
    $description = ( get_the_excerpt() ) ? get_the_excerpt() : get_bloginfo('description');
    $description = str_replace( '&nbsp;', ' ', $description );
    $description = preg_replace('!\s+!', ' ', $description);
    $description = trim( $description );
    $description = wp_trim_words( $description, 25, '...' );
    return $description;
}

// labrys_get_theme_thumbnail

function labrys_get_theme_thumbnail( $alt ) {
    $theme_thumbnail_array = array(
        'src' => get_template_directory_uri() .'/assets/img/thumbnail.png',
        'width' => '1200',
        'height' => '900',
        'is_intermediate' => false,
        'alt' => $alt,
        'caption' => ''
    );
    return $theme_thumbnail_array;
}

// labrys_get_post_thumbnail

function labrys_get_post_thumbnail( $post_id = false, $size = 'thumbnail', $fallback = 'parent', $theme_fallback = true ) {
    $post_thumbnail_array = array();
    $post_id = ( $post_id ) ? $post_id : get_the_ID();
    $post_title = ( $post_id ) ? get_the_title( $post_id ) : get_the_title();
    $post_title = ( $post_title ) ? $post_title : get_bloginfo('name');
    $thumbnail_id = ( $post_id ) ? get_post_thumbnail_id( $post_id ) : false;
    if ( $thumbnail_id ) { // get thumbnail
        $post_thumbnail_array = labrys_get_thumbnail( $thumbnail_id, $size );
    } else { // fallback
        switch ( $fallback ) { // fallback options
            case 'parent': // parent fallback
                $parent_id = wp_get_post_parent_id( $post_id );
                if ( $parent_id ) { // get parent thumbnail
                    $post_thumbnail_array = labrys_get_post_thumbnail( $parent_id, $size, 'parent', $theme_fallback );
                } else { // no parent, get frontpage thumbnail
                    $frontpage_id = get_option( 'page_on_front' );
                    $post_thumbnail_array = labrys_get_post_thumbnail( $frontpage_id, $size, 'parent', $theme_fallback );
                } // end no parent
                if ( !$post_thumbnail_array && $theme_fallback ) { // get default
                    $post_thumbnail_array = labrys_get_theme_thumbnail( $post_title );
                }
            break;
            case 'banner': // banner fallback
                if ( !$post_thumbnail_array ) { // post default
                    $parent_id = wp_get_post_parent_id( $post_id );
                    if ( $parent_id ) { // get parent thumbnail
                        $post_thumbnail_array = labrys_get_post_thumbnail( $parent_id, $size, 'parent', false );
                    } else {
                        $default_banner = get_field( 'banner_image', 'option' );
                        if ( $default_banner ) {
                            $post_thumbnail_array = labrys_get_thumbnail( $default_banner['ID'], $size );
                        }
                    }
                }
            break;
            case 'post': // post fallback
                // no fallback
            break;
            case 'social': // social fallback
                if ( !$post_thumbnail_array ) { // theme default
                    $post_thumbnail_array = labrys_get_theme_thumbnail( $post_title );
                }
            break;
            default: // thumbnail_id
                if ( is_numeric( $fallback ) ) { // get fallback from id passed
                    $thumbnail_id = intval( $fallback );
                    $post_thumbnail_array = labrys_get_thumbnail( $thumbnail_id, $size );
                    if ( !$post_thumbnail_array ) { // theme default
                        $post_thumbnail_array = labrys_get_theme_thumbnail( $post_title );
                    }
                }
            break;
        } // fallback options
    } // end fallback
    return $post_thumbnail_array;
}

// labrys_get_image_position

function labrys_get_image_position( $post_id = false ) {
    global $post;
    $post_id = ( $post_id ) ? $post_id : $post->ID;
    $image = get_field('image', $post_id);
    $image_position = $image['image_position'];
    if ( $image_position ) {
        return $image_position['image_position_x'] .'% '. $image_position['image_position_y'] .'%';
    } else {
        return false;
    }
}

function labrys_get_image( $image ) {
    $str = '';
    // defaults
    $color_palette = labrys_color_palette();
    $overlay_color = '#000000';
    $overlay_opacity = 0.5;
    $image_position = false;
    $parallax = false;
    // fields
    $image_id = $image['image_id'];
    if ($image_id) {
        if ( isset( $image['image_position'] ) ) {
            $image_position = ' data-object-position="'. $image['image_position']['image_position_x'] .'% '. $image['image_position']['image_position_y'] .'%"';
        }
        if ( isset( $image['overlay'] ) ) {
            $overlay_color = $image['overlay']['overlay_color'];
            $overlay_opacity = $image['overlay']['overlay_opacity'];
            $overlay_opacity = $overlay_opacity / 100;
        }
        if ( isset( $image['parallax'] ) ) {
            $parallax = $image['parallax'];
        }
        // image string
        $str .= '<div class="image-container object-fit'. ( $parallax ? ' parallax' : '' ) .'"'. $image_position .'>';
            $str .= wp_get_attachment_image( $image_id, 'full' );
        $str .= '</div>';
        // overlay
        $background = 'background: rgba('. implode(',', labrys_hex2rgb( $overlay_color ) ) .','. $overlay_opacity .');';
        $str .= '<div class="image-overlay" style="'. $background .'"></div>';
    }
    return $str;
}

// labrys_render_banner

function labrys_render_banner( $heading = false ) {
    global $post;
    $post_id = ( $post ) ? $post->ID : false;
    $str = '';
    // defaults
    $get_thumbnail_from_post_id = $post_id;
    $image = array();
    // post_type overrides
    $post_type = get_post_type($post);
    switch ( $post_type ) {
        case 'post':
        case 'page':
            $image = get_field('image');
        break;
    }
    $thumbnail = labrys_get_post_thumbnail( $get_thumbnail_from_post_id, 'full', 'banner' );
    $str .= '<div class="banner banner-top'. ( $thumbnail ? ' has-thumbnail' : '' ) .'">';
        if ( $thumbnail ) {
            $image['image_id'] = $thumbnail['id'];
            $str .= labrys_get_image( $image );
        }
        $str .= '<div class="banner-container">';
            $str .= '<div class="banner-row">';
                $str .= '<div class="banner-col">';
                    $str .= '<div class="banner-content">';
                        $str .= labrys_get_heading( $heading );
                    $str .= '</div>';
                $str .= '</div>';
            $str .= '</div>';
        $str .= '</div>';
    $str .= '</div>'."\n";
    echo $str;
}

// labrys_get_margin

function labrys_get_margin( $margin ) {
    $classes = array();
    if ( $margin ) {
        // margin_top
        if ( $margin['margin_top_overrides'] ) {
            foreach ( $margin['margin_top_overrides'] as $override ) {
                $br = $override['breakpoint'];
                $br = ( $br != 'xs' ) ? '-'. $br : '';
                $val = $override['margin_top'];
                $val = ( $val < 0 ) ? 'n'. abs($val) : $val;
                $classes[] = 'mt'. $br .'-'. $val;
            }
        }
        // margin_botttom
        if ( $margin['margin_bottom_overrides'] ) {
            foreach ( $margin['margin_bottom_overrides'] as $override ) {
                $br = $override['breakpoint'];
                $br = ( $br != 'xs' ) ? '-'. $br : '';
                $val = $override['margin_bottom'];
                $val = ( $val < 0 ) ? 'n'. abs($val) : $val;
                $classes[] = 'mb'. $br .'-'. $val;
            }
        }
    }
    if ( $classes ) {
        return ' '. implode(' ', $classes);
    } else {
        return false;
    }
}

// labrys_get_padding

function labrys_get_padding( $padding ) {
    $classes = array();
    if ( $padding ) {
        // padding_top
        if ( $padding['padding_top_overrides'] ) {
            foreach ( $padding['padding_top_overrides'] as $override ) {
                $br = $override['breakpoint'];
                $br = ( $br != 'xs' ) ? '-'. $br : '';
                $val = $override['padding_top'];
                $classes[] = 'pt'. $br .'-'. $val;
            }
        }
        // padding_botttom
        if ( $padding['padding_bottom_overrides'] ) {
            foreach ( $padding['padding_bottom_overrides'] as $override ) {
                $br = $override['breakpoint'];
                $br = ( $br != 'xs' ) ? '-'. $br : '';
                $val = $override['padding_bottom'];
                $classes[] = 'pb'. $br .'-'. $val;
            }
        }
    }
    if ( $classes ) {
        return ' '. implode(' ', $classes);
    } else {
        return false;
    }
}

// labrys_get_reusable_block

function labrys_get_reusable_block( $block_id = 0 ) {
    $block_id = intval($block_id);
    if ( $block_id ) {
        $content = get_post_field( 'post_content', $block_id );
        return apply_filters( 'the_content', $content );
    } else {
        return false;
    }
}

// labrys_get_post_feed

function labrys_get_post_feed( $feed_posts ) {
    $str = '';
    if ( is_array( $feed_posts ) && $feed_posts ) {
        $str .= '<div class="post-feed">';
        foreach ($feed_posts as $feed_post) {
            $str .= '<div class="feed-post">';
                $feed_post_permalink = get_permalink( $feed_post->ID );
                $str .= '<a href="'. $feed_post_permalink .'">';
                    $thumbnail = labrys_get_post_thumbnail( $feed_post->ID, 'medium_large', 'post' );
                    if ( $thumbnail ) {
                        $str .= '<div class="feed-post-thumbnail"><img src="'. $thumbnail['src'] .'" alt="'. $thumbnail['alt'] .'"></div>';
                    } else {
                        $str .= '<div class="feed-post-thumbnail no-thumbnail"></div>';
                    }
                    $str .= '<div class="feed-post-details">';
                        $str .= '<div class="feed-post-title">'. $feed_post->post_title .'</div>';
                        $str .= '<div class="feed-post-excerpt">'. $feed_post->post_excerpt .'</div>';
                        $str .= '<div class="feed-post-date">'. date('M j, Y', strtotime($feed_post->post_date)) .'</div>';
                    $str .= '</div>';
                $str .= '</a>';
            $str .= '</div>';
        }
        $str .= '</div>';
    }
    return $str;
}

// labrys_get_heading

function labrys_get_heading( $heading = false ) {
    global $post;
    $str = '';
    if ( !$heading ) {
        $heading = '<h1>'. get_the_title() .'</h1>';
        if ( $post ) {
            $custom_heading = get_field( 'custom_heading' );
            $heading = ( $custom_heading ) ? get_field( 'heading' ) : $heading;
        }
    }
    $str .= $heading ."\n";
    return $str;
}

// labrys_get_submenu

function labrys_get_submenu( $parent_id = false, $display_title = true, $submenu_orderby = 'menu_order', $submenu_order = 'ASC' ) {
    global $post;
    if ( !$parent_id ) { // root parent
        if ( $post->post_parent == 0 ) { // no parents
            $parent_id = $post->ID;
        } else { // get parent_id
            $parent_id = wp_get_post_parent_id( $post->ID );
            $ancestors = get_post_ancestors( $post->ID );
            $ancestor_count = count( $ancestors );
            if ( $ancestor_count >= 1 ) {
                $parent_id = $ancestors[ $ancestor_count - 1 ];
            }
        } // end get parent_id
    } // end root parent
    $parent = get_post( $parent_id );
    // get children
    $children = wp_list_pages('title_li=&sort_column='. $submenu_orderby .'&sort_order='. $submenu_order .'&child_of='. $parent_id .'&echo=0');
    // display menu
    $str = '';
    if ( $children ) {
        $str .= '<div class="submenu-container">';
            if ( $display_title ) {
                $title = $parent->post_title;
                $link = get_permalink( $parent_id );
                $str .= '<div class="submenu-title'. ( $parent_id == $post->ID ? ' current_page_item' : '' ) .'"><a href="'. $link .'">'. $title .'</a></div>';
            }
            $str .= '<ul class="submenu">';
                $str .= $children;
            $str .= '</ul>';
        $str .= '</div>';
    }
    return $str;
}

// labrys_get_sidebar

function labrys_get_sidebar( $content = '', $content_display = 'default' ) {
    global $post;
    $sidebar_content = $content;
    $sidebar_content_display = $content_display;
    $hide_submenu = false;
    $submenu_parent_id = false;
    $submenu_orderby = false;
    $submenu_order = false;
    $str = '';
    if ( $post ) {
        $sidebar_content .= get_field( 'sidebar_content' );
        $sidebar_content_display = get_field( 'sidebar_content_display' );
        $sidebar_content_display = (!$sidebar_content_display) ? $content_display : $sidebar_content_display;
        $hide_submenu = get_field( 'hide_submenu' );
        if ( !$hide_submenu ) {
            $submenu_orderby = get_field( 'submenu_orderby' );
            $submenu_order = get_field( 'submenu_order' );
            $submenu_root = get_field( 'submenu_root' );
            switch ( $submenu_root ) {
                case 'nearest':
                    $ancestors = get_post_ancestors( $post->ID );
                    if ($ancestors) {
                        $submenu_parent_id = $ancestors[0];
                    }
                break;
                case 'this':
                    $submenu_parent_id = $post->ID;
                break;
                case 'select':
                    $select_root_page = get_field( 'select_root_page' );
                    if ($select_root_page) {
                        $submenu_parent_id = $select_root_page->ID;
                    }
                break;
                default:
                    $submenu_parent_id = false;
                break;
            }
        }
    }
    $submenu = ( !$hide_submenu ) ? labrys_get_submenu( $submenu_parent_id, true, $submenu_orderby, $submenu_order ) : '';
    if ( $sidebar_content_display == 'above' ) {
        $str .= do_shortcode($sidebar_content);
    }
    $str .= $submenu;
    if ( $sidebar_content_display != 'above' ) {
        $str .= do_shortcode($sidebar_content);
    }
    return $str;
}

// labrys_get_contact

function labrys_get_contact( $order = array('phone', 'address', 'hours') ) {
    $str = '';
    $str .= '<div class="contact-container">';
        foreach ($order as $item) {
            switch ($item) {
                case 'email':
                    $contact_email = get_field( 'contact_email', 'option' );
                    if ( $contact_email ) {
                        $str .= '<div class="contact-email">';
                            $str .= '<div>';
                                $str .= '<div class="contact-row">';
                                    $str .= '<a href="mailto:'. $contact_email .'">'. $contact_email .'</a>';
                                $str .= '</div>';
                            $str .= '</div>';
                        $str .= '</div>';
                    }
                break;
                case 'phone':
                    $contact_phone = get_field( 'contact_phone', 'option' );
                    if ( $contact_phone ) {
                        $str .= '<div class="contact-phone">';
                            $str .= '<div>';
                                foreach ($contact_phone as $contact_row) {
                                    $str .= '<div class="contact-row">';
                                        $str .= ($contact_row['label']) ? '<span class="contact-label">'. $contact_row['label'] .'</span>' : '';
                                        $str .= '<a href="tel:'. preg_replace("/[^0-9]/", '', $contact_row['number'] ) .'">'. $contact_row['number'] .'</a>';
                                    $str .= '</div>';
                                }
                            $str .= '</div>';
                        $str .= '</div>';
                    }
                break;
                case 'address':
                    $contact_address = get_field( 'contact_address', 'option' );
                    if ( $contact_address ) {
                        $str .= '<div class="contact-address">';
                            $str .= '<div>';
                                for ($i = 0; $i < count($contact_address); $i++) {
                                    $str .= '<div class="contact-row">';
                                        $str .= ($i > 0) ? '<span class="comma">,</span> ' : '';
                                        $str .= $contact_address[$i]['address'];
                                    $str .= '</div>';
                                }
                            $str .= '</div>';
                        $str .= '</div>';
                    }
                break;
                case 'hours':
                    $contact_hours = get_field( 'contact_hours', 'option' );
                    $contact_hours_2 = get_field( 'contact_hours_2', 'option' );
                    if ( $contact_hours ) {
                        $str .= '<div class="contact-hours">';
                            $str .= '<div>';
                                for ($i = 0; $i < count($contact_hours); $i++) {
                                    $str .= '<div class="contact-row">';
                                        $str .= ($i > 0) ? '<span class="comma">,</span> ' : '';
                                        $str .= $contact_hours[$i]['hours'];
                                    $str .= '</div>';
                                }
                            $str .= '</div>';
                        $str .= '</div>';
                    }
                break;
            }
        }
    $str .= '</div>';
    return $str;
}

// labrys_get_social_links

function labrys_get_social_links() {
    $social_links = get_field( 'social_links', 'option' );
    $str = '';
    if ( $social_links ) {
        $str .= '<div class="social-links">'."\n";
            foreach ( $social_links as $social_link ) {
                $link_url = $social_link['social_link_url'];
                $link_text = $social_link['social_link_text'];
                $link_icon = '';
                if ( $social_link['social_link_icon_type'] == 'fa' ) {
                    $link_icon = '<span class="'. $social_link['social_link_icon_fa'] .' fa-2x"></span>';
                } elseif ( $social_link['social_link_icon_image'] ) {
                    $src = $social_link['social_link_icon_image']['sizes']['medium'];
                    $ext = pathinfo($src, PATHINFO_EXTENSION);
                    $link_icon = '<img src="'. $src .'" alt="'. $social_link['social_link_icon_image']['alt'] .'"'. ($ext == 'svg' ? ' class="style-svg"' : '') .'>';
                }
                $str .= '<div class="social-link">';
                    $str .= '<a href="'. $link_url .'" target="_blank">';
                        if ( $link_icon ) {
                            $str .= '<div class="social-link-icon">';
                                $str .= $link_icon;
                            $str .= '</div>';
                        }
                        if ( $link_text ) {
                            $str .= '<div class="social-link-text">';
                                $str .= $link_text;
                            $str .= '</div>';
                        }
                    $str .= '</a>';
                $str .= '</div>';
            }
        $str .= '</div>'."\n";
    }
    return $str;
}

// labrys_get_login_link

function labrys_get_login_link() {
    $str = '';
    $str .= '<div class="login-link">';
    if ( is_user_logged_in() ) {
        $str .= '<a href="'. home_url() .'/wp-login.php?action=logout">Log Out</a>';
    } else {
        '<a href="'. home_url() .'/login">Log In</a>';
    }
    $str .= '</div>';
    return $str;
}

// labrys_get_google_map

function labrys_get_google_map( $markers = array(), $zoom = false, $legend = array() ) {
    if ( !$markers ) { // default
        $marker = get_field('contact_location', 'option');
        $contact_address = get_field( 'contact_address', 'option' );
        $contact_address_2 = get_field( 'contact_address_2', 'option' );
        if ( $contact_address ) {
            $content = '<div>'. $contact_address . ( $contact_address_2 ? ',<br>'. $contact_address_2 : '' ) .'</div>';
            $marker['content'] = $content;
        }
        $markers = array($marker);
    }
    $zoom = ( $zoom ) ? $zoom : 11;
    $str = '';
    if ( $markers ) {
        $str .= '<div class="acf-map-container" data-zoom="'. $zoom .'">';
            foreach ($markers as $marker) {
                $lat = ' data-lat="'. esc_attr($marker['lat']) .'"';
                $lng = ' data-lng="'. esc_attr($marker['lng']) .'"';
                $icon = ( isset($marker['icon']) && $marker['icon'] ) ? ' data-icon="'. $marker['icon'] .'"' : '';
                $link = ( isset($marker['link']) && $marker['link'] ) ? ' data-link="'. $marker['link'] .'"' : '';
                $tooltip = ( isset($marker['tooltip']) && $marker['tooltip'] ) ? ' data-tooltip="'. $marker['tooltip'] .'"' : '';
                $str .= '<div class="marker"'. $lat . $lng . $icon . $link . $tooltip .'>';
                    if ( isset($marker['content']) ) {
                        $str .= $marker['content'];
                    }
                $str .= '</div>';
            }
        $str .= '</div>';
        if ( $legend ) {
            $img_path = get_template_directory_uri() .'/assets/img/';
            $str .= '<div class="map-legend">';
                $str .= '<div class="legend-bar-container">';
                    $str .= '<div class="legend-bar">';
                        $str .= '<div class="legend-heading">Legend</div>';
                        $str .= '<div class="legend-toggle"><i class="fas fa-caret-up"></i></div>';
                    $str .= '</div>';
                $str .= '</div>';
                $str .= '<div class="legend-content-container">';
                    $str .= '<div class="legend-content">';
                        foreach ($legend as $legend_row) {
                            $str .= '<div class="legend-row">';
                                $str .= '<div class="legend-marker">';
                                    $str .= '<img src="'. $legend_row['icon'] .'" alt="'. $legend_row['label'] .'">';
                                $str .= '</div>';
                                $str .= '<div class="legend-label">'. $legend_row['label'] .'</div>';
                            $str .= '</div>';
                        }
                    $str .= '</div>';
                $str .= '</div>';
            $str .= '</div>';
        }
    }
    return $str;
}

// labrys_check_redirect

function labrys_check_redirect() {
    global $post;
    if ( $post ) {
        $redirect_to_child = get_field( 'redirect_to_child', $post->ID );
        if ($redirect_to_child) {
            $children = get_pages('child_of='. $post->ID .'&sort_column=menu_order');
            if ($children) {
                $first_child = $children[0];
                wp_redirect( get_permalink( $first_child->ID ) );
                exit;
            }
        }
    }
}

// labrys_get_credit

function labrys_get_credit() {
    $str = '';
    $str .= '<div class="credit-container">';
        $str .= '<p><a href="https://shout-media.ca" target="_blank" rel="noopener">Website by shout-media.ca</a></p>';
    $str .= '</div>';
    return $str;
}

// labrys_get_hex_from_slug

function labrys_get_color_from_slug( $slug ) {
    $color_palette = labrys_color_palette();
    foreach ( $color_palette as $color ) {
        if ( $color['slug'] == $slug ) {
            return $color['color'];
        }
    }
    return false;
}

// labrys_hex2rgb

function labrys_hex2rgb( $hex ) {
    $hex = str_replace('#', '', $hex);
    if ( strlen($hex) == 3 ) {
        $r = hexdec(substr($hex,0,1).substr($hex,0,1));
        $g = hexdec(substr($hex,1,1).substr($hex,1,1));
        $b = hexdec(substr($hex,2,1).substr($hex,2,1));
    } else {
        $r = hexdec(substr($hex,0,2));
        $g = hexdec(substr($hex,2,2));
        $b = hexdec(substr($hex,4,2));
    }
    return array($r, $g, $b);
}

function labrys_google_api_key() {
    $default_google_api_key = 'AIzaSyDivzhX9fUCCqvaJWhbVXu-y1EDBCycwuU';
    $google_api_key = get_field('google_api_key', 'option');
    return ($google_api_key ? $google_api_key : $default_google_api_key);
}