<?php

function labrys_login_logo() {
    $custom_logo_id = get_theme_mod( 'custom_logo' );
    if ( $custom_logo_id ) {
        $logo_url = wp_get_attachment_image_url( $custom_logo_id, 'full' );
    } else {
        $logo_url = get_stylesheet_directory_uri() .'/assets/img/logo.svg';
    }
    ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo $logo_url; ?>);
            width: 320px;
            height: 70px;
            background-size: contain;
            background-repeat: no-repeat;
            background-position: center center;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'labrys_login_logo' );

function labrys_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'labrys_login_logo_url' );

function labrys_login_logo_url_title() {
    return 'Labrys';
}
add_filter( 'login_headertext', 'labrys_login_logo_url_title' );

// login style

function labrys_login_style() {
    wp_enqueue_style( 'labrys-login', get_template_directory_uri() .'/assets/css/login.css', array(), filemtime( get_stylesheet_directory() .'/assets/css/login.css' ) );
}
add_action( 'login_enqueue_scripts', 'labrys_login_style' );