<?php
/*
    Block Name: Grid
*/

    $grid_columns = get_field('grid_columns');
    $grid_auto_rows = get_field('grid_auto_rows');

    $class = 'acf-grid';
    $class .= ' '. $block['id'];
    if ( !empty($block['className']) ) {
        $class .= ' '. $block['className'];
    }
    if ( !empty($block['align']) ) {
        $class .= ' align'. $block['align'];
    }
    if ( $grid_columns ) {
        $class .= ' grid-columns-'. $grid_columns;
    }
    if ( $grid_auto_rows ) {
        $class .= ' grid-auto-rows';
    }

$str = '';

$allowed_blocks = array( 'acf/grid-item', 'acf/map' );

$str .= '<div class="'. esc_attr($class) .'">';
    $str .= '<InnerBlocks allowedBlocks="'. esc_attr( wp_json_encode( $allowed_blocks ) ) .'" />';
$str .= '</div>';

echo $str;
