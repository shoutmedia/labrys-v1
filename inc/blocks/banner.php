<?php
/*
    Block Name: Banner
*/

$image = get_field( 'image' );

$class = 'acf-banner';
$class .= ' '. $block['id'];
if ( !empty($block['className']) ) {
    $class .= ' '. $block['className'];
}
if ( !empty($block['align']) ) {
    $class .= ' align' . $block['align'];
}

$str = '';

$str .= '<div class="'. esc_attr($class) .'">';
    $str .= labrys_get_image( $image );
    $str .= '<div class="banner-container">';
        $str .= '<div class="banner-row">';
            $str .= '<div class="banner-col">';
                $str .= '<div class="banner-content">';
                    $str .= '<InnerBlocks />';
                $str .= '</div>';
            $str .= '</div>';
        $str .= '</div>';
    $str .= '</div>';
$str .= '</div>';

echo $str;
