<?php
/*
    Block Name: Post Feed
*/

    $max_posts = get_field('max_posts');
    $post_category = get_field('post_category');
    $date_range = get_field('date_range');
    $date_range_start = ( $date_range ) ? get_field('date_range_start') : false;
    $date_range_end = ( $date_range ) ? get_field('date_range_end') : false;
    $orderby = get_field('orderby');
    $order = get_field('order');
    $pin_posts = get_field('pin_posts');
    $post_select = get_field('post_select');
    $feed_posts = array();
    $post_type = 'post';

    $class = 'acf-post-feed';
    $class .= ' '. $block['id'];
    if ( !empty($block['className']) ) {
        $class .= ' '. $block['className'];
    }
    if ( !empty($block['align']) ) {
        $class .= ' align' . $block['align'];
    }

$str = '';

$str .= '<div class="'. esc_attr( $class ) .'">';

    // determine whether to use nopaging or posts_per_page
    if ( $max_posts < 0 ) {
        $nopaging = true;
        $posts_per_page = -1;
    } else {
        $nopaging = false;
        $posts_per_page = $max_posts;
    }

    // pinned posts
    $post__not_in = array();
    if ( $pin_posts && $post_select ) {
        if ( $posts_per_page >= 0 ) { // limit pinned posts
            $post_select = array_slice( $post_select, 0, $posts_per_page );
            $posts_per_page -= count( $post_select );
            if ( $posts_per_page < 0 ) {
                $posts_per_page = 0;
            }
        }
        $feed_posts = $post_select; // set feed posts
        foreach ( $feed_posts as $feed_post ) { // remove from query
            $post__not_in[] = $feed_post->ID;
        }
    }

    // WP_Query
    if ( $posts_per_page != 0 ) {
        $args = array(
            'post_type' => $post_type,
            'nopaging' => $nopaging,
            'posts_per_page' => $posts_per_page,
            'orderby' => $orderby,
            'order' => $order,
        );
        if ( $post__not_in ) { // exclude selected posts
            $args['post__not_in'] = $post__not_in;
        }
        // tax_query
        if ( $post_category ) {
            $tax_query = array(
                'taxonomy' => 'category',
                'field' => 'term_id',
                'terms' => $post_category
            );
            $args['tax_query'] = array( $tax_query );
        }
        // determine whether to use date_query
        if ( $date_range ) {
            $date_query = array( 'inclusive' => true );
            if ( $date_range_start && $date_range_end ) {
                if ( $date_range_end < $date_range_start ) {
                    $date_range_end = false;
                }
            }
            if ( $date_range_start ) {
                $date_query['after'] = $date_range_start;
            }
            if ( $date_range_end ) {
                $date_query['before'] = $date_range_end;
            }
            $args['date_query'] = array( $date_query );
        }
        // query
        $query = new WP_Query( $args );
        $feed_posts = array_merge( $feed_posts, $query->posts );
        wp_reset_postdata();
    }
    
    // display $feed_posts
    if ( $feed_posts ) {
        $str .= labrys_get_post_feed( $feed_posts );
    }

$str .= '</div>';

echo $str;