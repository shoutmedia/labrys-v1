<?php
/*
    Block Name: Cards
*/

    $cards = get_field('cards');

    $class = 'acf-cards';
    $class .= ' '. $block['id'];
    if ( !empty($block['className']) ) {
        $class .= ' '. $block['className'];
    }
    if ( !empty($block['align']) ) {
        $class .= ' align' . $block['align'];
    }

    // get background colors
    if ( $cards ) {
        $first_card = $cards[0];
        $first_color = labrys_get_color_from_slug( $first_card['color_select'] );
        $last_card = $cards[count($cards) - 1];
        $last_color = labrys_get_color_from_slug( $last_card['color_select'] );
    }

$str = '';

$str .= '<div class="'. esc_attr( $class ) .'" style="background: linear-gradient(90deg, '. $first_color .' 50%, '. $last_color .' 50%);">';
    $str .= '<div class="card-container">';
        foreach ( $cards as $card ) {
            $card_link = ( $card['link_type'] == 'page' ) ? $card['link_page'] : $card['link_url'];
            $icon = '';
            if ( $card['icon_type'] == 'fa' ) {
                $icon = '<span class="'. $card['icon_fa'] .' fa-5x"></span>';
            } elseif ( $card['icon_image'] ) {
                $src = $card['icon_image']['sizes']['medium'];
                $ext = pathinfo($src, PATHINFO_EXTENSION);
                $icon = '<img src="'. $src .'" alt="'. $card['icon_image']['alt'] .'"'. ($ext == 'svg' ? ' class="style-svg"' : '') .'>';
            }
            $str .= '<div class="card'. ($card['color_select'] ? ' card-'. $card['color_select'] : '') .'">';
                $str .= ($card_link) ? '<a href="'. $card_link .'"'. ( $card['link_type'] == 'url' ? ' target="_blank"' : '' ) .'>' : '<span>';
                    if ( $icon ) {
                        $str .= '<div class="icon">';
                            $str .= $icon;
                        $str .= '</div>';
                    }
                    if ($card['heading']) {
                        $str .= '<h3>' . $card['heading'] . '</h3>';
                    }
                    if ($card['description']) {
                        $str .= '<p>'. $card['description'] .'</p>';
                    }
                $str .= ($card_link) ? '</a>' : '</span>';
            $str .= '</div>';
        }
    $str .= '</div>';
$str .= '</div>';

echo $str;
