<?php
/*
    Block Name: Flex Container
*/
    $has_background_color = get_field('has_background_color');
    $background_color = get_field('color_select');
    $has_background_image = get_field('has_background_image');
    $image = get_field('image');
    $margin = get_field('margin');
    $padding = get_field('padding');
    $row_justify = get_field('row_justify');
    $row_align = get_field('row_align');
    $row_margin = get_field('row_margin');
    $row_direction = get_field('row_direction');

    // class

    $class = 'acf-flex-container';
    $class .= ' '. $block['id'];
    if ( !empty($block['className']) ) {
        $class .= ' '. $block['className'];
    }
    if ( !empty($block['align']) ) {
        $class .= ' align' . $block['align'];
    }
    if ($has_background_color || $has_background_image) {
        $class .= ' has-background';
    }
    if ($has_background_color && $background_color) {
        $class .= ' has-'. $background_color .'-background-color';
    }
    if ($has_background_image) {
        $class .= ' has-background-image';
    }
    if ($margin) {
        $class .= labrys_get_margin($margin);
    }
    if ($padding) {
        $class .= labrys_get_padding($padding);
    }

    // container
    $container = '';
    if ($block['align'] == 'full') {
        $container .= 'container-fluid';
    } elseif ($block['align'] == 'wide') {
        $container .= 'container';
    }

    // row_classes

    $row_classes = array();

    // row_justify
    if ( $row_justify ) {
        foreach ($row_justify as $row_justify_row) {
            $br = $row_justify_row['breakpoint_group']['breakpoint'];
            $br = ($br != 'xs') ? '-'. $br : '';
            $j = $row_justify_row['justify_group']['justify'];
            $row_classes[] = 'justify-content'. $br .'-'. $j;
        }
    }

    // row_align
    if ( $row_align ) {
        foreach ($row_align as $row_align_row) {
            $br = $row_align_row['breakpoint_group']['breakpoint'];
            $br = ($br != 'xs') ? '-'. $br : '';
            $a = $row_align_row['align_group']['align'];
            $row_classes[] = 'align-items'. $br .'-'. $a;
        }
    }

    // row_margin
    if ( $row_margin ) {
        foreach ($row_margin as $row_margin_row) {
            $br = $row_margin_row['breakpoint_group']['breakpoint'];
            $br = ($br != 'xs') ? '-'. $br : '';
            $m = $row_margin_row['margin_group']['margin'];
            $m = ($m) ? '0' : 'n4';
            $row_classes[] = 'mx'. $br .'-'. $m;
        }
    }

    // row_direction
    if ( $row_direction ) {
        foreach ($row_direction as $row_direction_row) {
            $br = $row_direction_row['breakpoint_group']['breakpoint'];
            $br = ($br != 'xs') ? '-'. $br : '';
            $d = $row_direction_row['direction_group']['direction'];
            $row_classes[] = 'flex'. $br .'-'. $d;
        }
    }

$str = '';

$allowed_blocks = array( 'acf/flex-column' );

$str .= '<div class="'. esc_attr($class) .'">';
    $str .= ( $has_background_image ) ? labrys_get_image( $image ) : '';
    $str .= ($container) ? '<div class="'. $container .'">' : '';
        $str .= '<div class="row'. ( $row_classes ? ' '. esc_attr( implode(' ', $row_classes) ) : '') .'">';
            $str .= '<InnerBlocks allowedBlocks="'. esc_attr( wp_json_encode( $allowed_blocks ) ) .'" />';
        $str .= '</div>';
    $str .= ($container) ? '</div>' : '';
$str .= '</div>';

echo $str;
