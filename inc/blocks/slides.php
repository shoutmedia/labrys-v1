<?php
/*
    Block Name: Slides
*/

    $slides = get_field('slides');

    $class = 'acf-slides';
    $class .= ' '. $block['id'];
    if ( !empty($block['className']) ) {
        $class .= ' '. $block['className'];
    }
    if ( !empty($block['align']) ) {
        $class .= ' align' . $block['align'];
    }

?>

<div class="<?php echo esc_attr($class); ?>">
    <?php for ($i = 0; $i < count($slides); $i++) {

        $slide = $slides[$i];
        $image_class = 'image-'. ($i + 1);

        // content
        $slide_text = $slide['slide_text'];
        $slide_image = $slide['slide_image'];
        $slide_image_mobile = $slide['slide_image_mobile'];

        // src
        $lg_src = $slide_image['sizes']['large'];
        $sm_src = ( $slide_image_mobile ) ? $slide_image_mobile['sizes']['large'] : $lg_src;

        // settings
        $slide_color = $slide['color_picker'];
        $image_alignment = $slide['image_alignment'];
        $image_alignment_mobile = $slide['image_alignment_mobile'];

        $parallax = true; // apply parallax

    ?>
        <div class="slide-container">
            <div class="slide<?php echo ($slide_color ? ' slide-color-'. $slide_color : ''); ?>">
                <div class="image-container<?php echo ( !empty( $slide_text ) ? ' has-text' : ' no-text') . ( $parallax ? ' parallax-slide' : '' ); ?>">
                    <div class="image<?php echo ' '. $image_class; ?>">
                        <picture>
                            <source media="(max-width: 991.98px)" srcset="<?php echo $sm_src; ?>">
                            <img src="<?php echo $lg_src; ?>" alt="<?php echo $slide_image['alt']; ?>">
                        </picture>
                    </div>
                    <?php
                        echo '<style media="screen">'."\n";
                            echo 'body .acf-slides.'. $block['id'] .' .'. $image_class .' picture img {'."\n";
                                echo 'object-position: '. $image_alignment_mobile['horizontal_alignment'] .'% '. $image_alignment_mobile['vertical_alignment'] .'%;'."\n";
                            echo '}'."\n";
                            echo '@media (min-width: 992px) {'."\n";
                                echo 'body .acf-slides.'. $block['id'] .' .'. $image_class .' picture img {'."\n";
                                    echo 'object-position: '. $image_alignment['horizontal_alignment'] .'% '. $image_alignment['vertical_alignment'] .'%;'."\n";
                                echo '}'."\n";
                            echo '}'."\n";
                        echo '</style>'."\n";
                    ?>
                </div>
                <?php if (!empty($slide_text)) { ?>
                    <div class="content-container">
                        <div class="content">
                            <div class="slide-text"><?php echo $slide_text; ?></div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    <?php } // end for ?>
</div>
