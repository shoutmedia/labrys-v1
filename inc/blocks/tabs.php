<?php
/*
    Block Name: Tabs
*/

    $tabs = get_field('tabs');
    $id = $block['id'];

    $class = 'acf-tabs';
    $class .= ' '. $block['id'];
    if ( !empty($block['className']) ) {
        $class .= ' '. $block['className'];
    }
    if ( !empty($block['align']) ) {
        $class .= ' align' . $block['align'];
    }

$str = '';

$str .= '<div id="'. $id .'" class="'. esc_attr( $class ) .'">';
    // tab
    $str .= '<div class="tab-container">';
        $str .= '<div class="tab-list" role="tablist" aria-label="Tabs">';
            for ( $i = 0; $i < count($tabs); $i++ ) {
                $tab = $tabs[$i];
                if ( $tab['name'] ) {
                    $selected = ($i === 0) ? true : false;
                    $str .= '<button class="tab-btn'. ( $selected ? ' selected' : '' ) .'" role="tab" aria-controls="'. $id .'-panel-'. $i .'" id="'. $id .'-tab-'. $i .'"'. ( $selected ? '  aria-selected="true" tabindex="0"' : 'aria-selected="false" tabindex="-1"' ) .'>';
                        $str .= $tab['name'];
                    $str .= '</button>';
                }
            }
        $str .= '</div>';
    $str .= '</div>';
    // content
    $str .= '<div class="tab-content-container">';
        for ( $i = 0; $i < count($tabs); $i++ ) {
            $tab = $tabs[$i];
            if ( $tab['name'] ) {
                $selected = ($i === 0) ? true : false;
                $str .= '<div class="tab-content" role="tabpanel" aria-labelledby="'. $id .'-tab-'. $i .'" id="'. $id .'-panel-'. $i .'"'. ( $selected ? '' : ' hidden="hidden"' ) .'>';
                    if ( $tab['content'] ) {
                        $str .= $tab['content'];
                    }
                $str .= '</div>';
            }
        }
    $str .= '</div>';
$str .= '</div>';

echo $str;
