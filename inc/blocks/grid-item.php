<?php
/*
    Block Name: Grid Item
*/
    $has_background_color = get_field('has_background_color');
    $background_color = get_field('color_select');
    $has_background_image = get_field('has_background_image');
    $image = get_field('image');
    $overrides = get_field( 'overrides' );

    // class

    $class = 'acf-grid-item';
    $class .= ' '. $block['id'];
    if ( !empty($block['className']) ) {
        $class .= ' '. $block['className'];
    }
    if ( !empty($block['align']) ) {
        $class .= ' align' . $block['align'];
    }
    if ($has_background_color || $has_background_image) {
        $class .= ' has-background';
    }
    if ($has_background_color && $background_color) {
        $class .= ' has-'. $background_color .'-background-color';
    }
    if ($has_background_image) {
        $class .= ' has-background-image';
    }
    if ($overrides) {
        foreach ($overrides as $override) {
            $br = (isset($override['breakpoint'])) ? $override['breakpoint']['desktop_breakpoint'] : false;
            if ($br) {
                // grid column
                $grid_column = $override['grid_column'];
                if ($grid_column['grid_column_override']) {
                    $class .= ' grid-col-start-'. $br .'-'. $grid_column['grid_column_start'];
                    $class .= ' grid-col-span-'. $br .'-'. $grid_column['grid_column_span'];
                }
                // grid_row
                $grid_row = $override['grid_row'];
                if ($grid_row['grid_row_override']) {
                    $class .= ' grid-row-start-'. $br .'-'. $grid_column['grid_row_start'];
                    $class .= ' grid-row-span-'. $br .'-'. $grid_column['grid_row_span'];
                }
                // content
                if ($override['content_justify']) {
                    $class .= ' content-justify-'. $br .'-'. $override['content_justify'];
                }
                if ($override['content_align']) {
                    $class .= ' content-align-'. $br .'-'. $override['content_align'];
                }
            }
        }
    }

$str = '';

$str .= '<div class="'. esc_attr($class) .'">';
    $str .= '<div class="grid-item-container">';
        $str .= ( $has_background_image ) ? labrys_get_image( $image ) : '';
        $str .= '<div class="grid-item-content">';
            $str .= '<InnerBlocks />';
        $str .= '</div>';
    $str .= '</div>';
$str .= '</div>';

echo $str;
