<?php
/*
    Block Name: Flex Column
*/
    $has_background_color = get_field('has_background_color');
    $background_color = get_field('color_select');
    $has_background_image = get_field('has_background_image');
    $image = get_field('image');
    $column_width = get_field('column_width');
    $column_offset = get_field('column_offset');
    $column_order = get_field('column_order');
    $content_flex = get_field('content_flex');
    $content_justify = get_field('content_justify');
    $content_align = get_field('content_align');

    // class

    $class = 'acf-flex-column';
    $class .= ' '. $block['id'];
    if ( !empty($block['className']) ) {
        $class .= ' '. $block['className'];
    }
    if ( !empty($block['align']) ) {
        $class .= ' align' . $block['align'];
    }
    if ($has_background_color || $has_background_image) {
        $class .= ' has-background';
    }
    if ($has_background_color && $background_color) {
        $class .= ' has-'. $background_color .'-background-color';
    }
    if ($has_background_image) {
        $class .= ' has-background-image';
    }

    // col_classes

    $col_classes = array();

    // column_width
    if ( $column_width ) {
        foreach ($column_width as $column_width_row) {
            $br = $column_width_row['breakpoint_group']['breakpoint'];
            $br = ($br != 'xs') ? '-'. $br : '';
            $w = $column_width_row['width_group']['width'];
            if ($w > 0) {
                $w = '-'. $w;
            } else {
                $auto = $column_width_row['width_group']['auto'];
                $w = ($auto) ? '-auto' : '';
            }
            $col_classes[] = 'col'. $br . $w;
        }
    }
    
    // column_offset
    if ( $column_offset ) {
        foreach ($column_offset as $column_offset_row) {
            $br = $column_offset_row['breakpoint_group']['breakpoint'];
            $br = ($br != 'xs') ? '-'. $br : '';
            $o = $column_offset_row['offset_group']['offset'];
            $col_classes[] = 'offset'. $br .'-'. $o;
        }
    }

    // column_order
    if ( $column_order ) {
        foreach ($column_order as $column_order_row) {
            $br = $column_order_row['breakpoint_group']['breakpoint'];
            $br = ($br != 'xs') ? '-'. $br : '';
            $o = $column_order_row['order_group']['order'];
            $col_classes[] = 'order'. $br .'-'. $o;
        }
    }

    if ( $col_classes ) {
        $class .= ' '. implode(' ', $col_classes);
    } else {
        $class .= ' col';
    }

    // content_classes

    $content_classes = array();

    // content_flex
    if ( $content_flex ) {
        foreach ($content_flex as $content_flex_row) {
            $br = $content_flex_row['breakpoint_group']['breakpoint'];
            $br = ($br != 'xs') ? '-'. $br : '';
            $f = $content_flex_row['flex_group']['flex'];
            $f = ($f) ? 'flex' : 'block';
            $content_classes[] = 'd'. $br .'-'. $f;
        }
    }

    // content_justify
    if ( $content_justify ) {
        foreach ($content_justify as $content_justify_row) {
            $br = $content_justify_row['breakpoint_group']['breakpoint'];
            $br = ($br != 'xs') ? '-'. $br : '';
            $j = $content_justify_row['justify_group']['justify'];
            $content_classes[] = 'justify-content'. $br .'-'. $j;
        }
    }

    // content_align
    if ( $content_align ) {
        foreach ($content_align as $content_align_row) {
            $br = $content_align_row['breakpoint_group']['breakpoint'];
            $br = ($br != 'xs') ? '-'. $br : '';
            $a = $content_align_row['align_group']['align'];
            $content_classes[] = 'align-items'. $br .'-'. $a;
        }
    }

$str = '';

$str .= '<div class="'. esc_attr($class) .'">';
    $str .= ( $has_background_image ) ? labrys_get_image( $image ) : '';
    $str .= '<div class="flex-column-container'. ( $content_classes ? ' '. esc_attr( implode(' ', $content_classes) ) : '') .'">';
        $str .= '<div class="flex-column-content">';
            $str .= '<InnerBlocks />';
        $str .= '</div>';
    $str .= '</div>';
$str .= '</div>';

echo $str;