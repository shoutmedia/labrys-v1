<?php
/*
    Block Name: Section
*/
    $has_background_color = get_field('has_background_color');
    $background_color = get_field('color_select');
    $has_background_image = get_field('has_background_image');
    $image = get_field('image');
    $margin = get_field('margin');
    $padding = get_field('padding');

    // class

    $class = 'acf-section';
    $class .= ' '. $block['id'];
    if ( !empty($block['className']) ) {
        $class .= ' '. $block['className'];
    }
    if ( !empty($block['align']) ) {
        $class .= ' align' . $block['align'];
    }
    if ($has_background_color || $has_background_image) {
        $class .= ' has-background';
    }
    if ($has_background_color && $background_color) {
        $class .= ' has-'. $background_color .'-background-color';
    }
    if ($has_background_image) {
        $class .= ' has-background-image';
    }
    if ($margin) {
        $class .= labrys_get_margin($margin);
    }
    if ($padding) {
        $class .= labrys_get_padding($padding);
    }

    // style

$str = '';

$str .= '<div class="'. esc_attr($class) .'">';
$str .= ( $has_background_image ) ? labrys_get_image( $image ) : '';
    $str .= '<div class="section-container">';
        $str .= '<div class="section-content">';
            $str .= '<InnerBlocks />';
        $str .= '</div>';
    $str .= '</div>';
$str .= '</div>';

echo $str;