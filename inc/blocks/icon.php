<?php
/*
    Block Name: Icon
*/

    $link_type = get_field('link_type');
    $link_page = get_field('link_page');
    $link_url = get_field('link_url');
    $icon_type = get_field('icon_type');
    $icon_fa = get_field('icon_fa');
    $icon_image = get_field('icon_image');
    $color_select = get_field('color_select');

    $class = 'acf-icon-block';
    $class .= ' '. $block['id'];
    if ( !empty($block['className']) ) {
        $class .= ' '. $block['className'];
    }
    if ($color_select) {
        $class .= ' icon-'. $color_select;
    }

$str = '';

$str .= '<div class="'. esc_attr( $class ) .'">';
    $str .= '<div class="icon-container">';
        $icon_link = ( $link_type == 'page' ) ? $link_page : $link_url;
        $str .= ($icon_link) ? '<a href="'. $icon_link .'"'. ( $link_type == 'url' ? ' target="_blank"' : '' ) .'>' : '<span>';
            $str .= '<div class="icon">';
            if ( $icon_type == 'fa' ) {
                $str .= '<span class="'. $icon_fa .' fa-5x"></span>';
            } elseif ( $icon_image ) {
                $src = $icon_image['sizes']['medium'];
                $ext = pathinfo($src, PATHINFO_EXTENSION);
                $str .= '<img src="'. $src .'" alt="'. $icon_image['alt'] .'"'. ($ext == 'svg' ? ' class="style-svg"' : '') .'>';
            }
            $str .= '</div>';
        $str .= ($icon_link) ? '</a>' : '</span>';
    $str .= '</div>';
$str .= '</div>';

echo $str;
