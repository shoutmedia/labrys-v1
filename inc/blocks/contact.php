<?php
/*
    Block Name: Contact
*/

    $class = 'acf-contact';
    $class .= ' '. $block['id'];
    if ( !empty($block['className']) ) {
        $class .= ' '. $block['className'];
    }

$str = '';

$str .= '<div class="'. esc_attr( $class ) .'">';
    $str .= '<div class="contact-container">';
        // hours
        $contact_hours = get_field( 'contact_hours', 'option' );
        $contact_hours_2 = get_field( 'contact_hours_2', 'option' );
        if ( $contact_hours ) {
            $str .= '<h2 class="contact-hours">'. __('Hours of Operation:') .'</h2>';
            $str .= '<div>';
            $str .= $contact_hours;
            if ( $contact_hours_2 ) {
                $str .= '<span class="comma">,</span> <span class="contact-hours-2">'. $contact_hours_2 .'</span>';
            }
            $str .= '</div>';
        }
        // address
        $contact_address = get_field( 'contact_address', 'option' );
        $contact_address_2 = get_field( 'contact_address_2', 'option' );if ( $contact_address ) {
            $str .= '<h2 class="contact-address">'. __('Location:') .'</h2>';
            $str .= '<div>';
            $str .= $contact_address;
            if ( $contact_address_2 ) {
                $str .= '<span class="comma">,</span> <span class="contact-address-2">'. $contact_address_2 .'</span>';
            }
            $str .= '</div>';
        }
        // phone
        $contact_phone = get_field( 'contact_phone', 'option' );
        if ( $contact_phone ) {
            $str .= '<h2 class="contact-phone">'. __('Phone Number:') .'</h2>';
            $str .= '<div>';
                $str .= '<a href="tel:'. preg_replace("/[^0-9]/", '', $contact_phone ) .'">'. $contact_phone .'</a>';
            $str .= '</div>';
        }
    $str .= '</div>';
$str .= '</div>';

echo $str;
