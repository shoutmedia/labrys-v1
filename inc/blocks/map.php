<?php
/*
    Block Name: Map
*/

    $use_default_location = get_field('use_default_location');
    $markers = get_field('markers');
    $zoom = get_field('zoom');
    $legend = get_field('legend');

    $class = 'acf-map';
    $class .= ' '. $block['id'];
    if ( !empty($block['className']) ) {
        $class .= ' '. $block['className'];
    }
    if ( !empty($block['align']) ) {
        $class .= ' align' . $block['align'];
    }
    $class .= ' has-background';

    if ( $use_default_location ) {
        $markers = array();
    } else {
        if ( $markers ) {
            for ($i = 0; $i < count($markers); $i++) {
                $markers[$i]['lat'] = $markers[$i]['location']['lat'];
                $markers[$i]['lng'] = $markers[$i]['location']['lng'];
            }
        }
    }

$str = '';

$str .= '<div class="'. esc_attr( $class ) .'">';
    $str .= labrys_get_google_map( $markers, $zoom, $legend );
$str .= '</div>';

echo $str;