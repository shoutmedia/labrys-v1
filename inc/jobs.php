<?php

function labrys_register_jobs_post_type() {
    // jobs
    $args = array(
        'labels'            => labrys_get_post_type_labels( 'Job', 'Jobs' ),
        'rewrite'           => array( 'slug' => 'job' ),
        'supports'          => array( 'title' ),
        'hierarchical'      => false,
        'public'            => true,
        'show_ui'           => true,
        'show_in_rest'      => true,
        'menu_icon'         => 'dashicons-businessperson',
        'menu_position'     => 20
    );
    register_post_type( 'labrys-jobs', $args );
}
add_action( 'init', 'labrys_register_jobs_post_type' );

function labrys_get_job_content( $job_id, $apply_btn = false ) {
    $str = '';
    $str .= '<div class="job-content">';
        $str .= '<div class="job-row">';
            $str .= '<div class="job-col-1">';
                $str .= '<h3>'. get_the_title( $job_id ) .'</h3>';
                $city = get_field('city', $job_id);
                if ($city) {
                    $str .= '<p class="job-city"><strong>'. $city .'</strong></p>';
                }
                $job_type = get_field('job_type', $job_id);
                if ($job_type) {
                    $str .= '<p class="job-type"><strong>'. $job_type .'</strong></p>';
                }
                if ( $apply_btn ) {
                    $str .= '<p class="mt-4">';
                        $str .= '<a href="'. get_permalink( $job_id ) .'" class="button has-color-2-background-color is-style-arrow">Apply Now</a>';
                    $str .= '</p>';
                }
            $str .= '</div>';
            $str .= '<div class="job-col-2">';
                $content = get_field('content', $job_id);
                $str .= $content;
            $str .= '</div>';
        $str .= '</div>';
    $str .= '</div>';
    return $str;
}