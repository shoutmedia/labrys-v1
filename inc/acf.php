<?php

/////////
// ACF //
/////////

// options pages

if ( function_exists('acf_add_options_page') ) {

    acf_add_options_page(
        array(
            'page_title'    => 'Theme Settings',
            'menu_slug'     => 'labrys-settings',
            'capability'    => 'edit_theme_options',
            'redirect'      => 'labrys-contact',
            'position'      => 59
        )
    );

    acf_add_options_sub_page(
        array(
            'page_title'    => 'Contact',
            'menu_slug'     => 'labrys-contact',
            'parent_slug'   => 'labrys-settings',
        )
    );

    acf_add_options_sub_page(
        array(
            'page_title'    => 'Banner',
            'menu_slug'     => 'labrys-banner',
            'parent_slug'   => 'labrys-settings',
        )
    );

    acf_add_options_sub_page(
        array(
            'page_title'    => 'Social',
            'menu_slug'     => 'labrys-social',
            'parent_slug'   => 'labrys-settings',
        )
    );

    acf_add_options_sub_page(
        array(
            'page_title'    => 'Footer',
            'menu_slug'     => 'labrys-footer',
            'parent_slug'   => 'labrys-settings',
        )
    );

    acf_add_options_sub_page(
        array(
            'page_title'    => 'Tracking',
            'menu_slug'     => 'labrys-tracking',
            'parent_slug'   => 'labrys-settings',
        )
    );

}
  
// add default image setting to ACF image fields

function labrys_add_default_value_to_image_field($field) {
    acf_render_field_setting( $field, array(
        'label'			=> 'Default Image',
        'instructions'	=> 'Appears when creating a new post',
        'type'			=> 'image',
        'name'			=> 'default_value',
    ));
}
add_action('acf/render_field_settings/type=image', 'labrys_add_default_value_to_image_field');

// TinyMCE

// Add styleselect to editors

function labrys_acf_toolbars( $toolbars ) {
    array_shift( $toolbars['Full'][1] );
    array_unshift( $toolbars['Full'][1], 'formatselect', 'styleselect' );
    $toolbars['Custom'] = $toolbars['Basic'];
    array_unshift( $toolbars['Custom'][1], 'formatselect', 'styleselect' );
    return $toolbars;
}
add_filter( 'acf/fields/wysiwyg/toolbars', 'labrys_acf_toolbars' );

// TinyMCE Styles

function labrys_mce_before_init( $settings ) {
    $colors = labrys_color_palette();
    $style_formats = array();
    foreach ($colors as $color) {
        $style_formats[] = array(
            'title' => $color['name'],
            'inline' => 'span',
            'classes' => 'has-'. $color['slug'] .'-color',
            'wrapper' => true
        );
    }
    array_push( $style_formats,
        array(
            'title' => 'Button',
            'selector' => 'a',
            'classes' => 'btn'
        ),
        array(
            'title' => 'Size Small',
            'inline' => 'span',
            'classes' => 'text-small',
            'wrapper' => true
        ),
        array(
            'title' => 'Size Large',
            'inline' => 'span',
            'classes' => 'text-large',
            'wrapper' => true
        ),
        array(
            'title' => 'Weight Regular',
            'inline' => 'span',
            'classes' => 'text-regular',
            'wrapper' => true
        ),
        array(
            'title' => 'Weight Bold',
            'inline' => 'span',
            'classes' => 'text-bold',
            'wrapper' => true
        ),
        array(
            'title' => 'Weight Black',
            'inline' => 'span',
            'classes' => 'text-black',
            'wrapper' => true
        ),
        array(
            'title' => 'No Wrap',
            'inline' => 'span',
            'classes' => 'text-nowrap',
            'wrapper' => true
        )
    );
    $settings['style_formats'] = json_encode( $style_formats );
    $settings['preview_styles'] = false;
    $settings['cache_suffix'] = 'v='. time();
    return $settings;
}
add_filter( 'tiny_mce_before_init', 'labrys_mce_before_init' );

// labrys_acf_color_picker_palette

function labrys_acf_color_picker_palette() {
    $color_palette = labrys_color_palette();
    $color_str = '';
    for ( $i = 0; $i < count($color_palette); $i++ ) {
        $color_str .= ($i > 0) ? ', ' : '';
        $color_str .= "'". $color_palette[$i]['color'] ."'";
    }
    ?>
    <script type="text/javascript">
        (function($) { 
            acf.add_filter('color_picker_args', function( args, $field ) {
                args.palettes = [<?php echo $color_str; ?>];
                return args;      
            });
        })(jQuery); 
    </script>
    <?php      
}
add_action('acf/input/admin_footer', 'labrys_acf_color_picker_palette');

// google maps key

function labrys_acf_google_api_key() {
	acf_update_setting('google_api_key', labrys_google_api_key() );
}
add_action('acf/init', 'labrys_acf_google_api_key');