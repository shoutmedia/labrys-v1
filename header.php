<?php labrys_check_redirect(); ?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="theme-color" content="#ffffff">
    <style media="screen">
        body {
            opacity: 0;
            transition: 1s ease opacity;
        }
    </style>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="mobile-container" class="top">
    <div class="menu-bar">
        <div class="container">
            <div class="row">
                <div class="logo-container header-logo">
                    <a class="logo" href="<?php echo home_url(); ?>">
                        <?php echo labrys_get_logo('white'); ?>
                    </a>
                </div>
                <div class="col navigation-container">
                    <a class="menu-toggle mobile-icon" href="#"><span class="fal fa-times fa-fw"></span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col">
                <nav class="mobile-nav">
                    <?php
                        if ( has_nav_menu( 'main' ) ) {
                            wp_nav_menu(
                                array(
                                    'theme_location'    => 'main',
                                    'container_class'   => 'menu-container',
                                    'menu_class'        => 'menu'
                                )
                            );
                        }
                    ?>
                    <div class="nav-contact">
                        <?php echo labrys_get_contact(); ?>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>
<div id="container">
    <header id="header" class="top">
        <div class="menu-bar">
            <div class="container">
                <div class="row">
                    <div class="logo-container header-logo">
                        <a class="logo" href="<?php echo home_url(); ?>">
                            <?php echo labrys_get_logo('white'); ?>
                        </a>
                    </div>
                    <div class="col navigation-container">
                        <a class="menu-toggle mobile-icon" href="#"><span class="fal fa-bars fa-fw"></span></a>
                        <nav class="main-nav">
                            <?php
                                if ( has_nav_menu( 'main' ) ) {
                                    wp_nav_menu(
                                        array(
                                            'theme_location'    => 'main',
                                            'container_class'   => 'menu-container',
                                            'menu_class'        => 'menu'
                                        )
                                    );
                                }
                            ?>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </header>
