// =========================================================
// gulpfile.js
// =========================================================

var theme_dir = '.';

var css_dir = theme_dir +'/';
var assets_dir = theme_dir +'/assets';
var assets_css_dir = assets_dir +'/css';
var js_dir = assets_dir +'/js';
var lib_dir = assets_dir +'/lib';
var maps_dir = assets_dir +'/maps';

var src_dir = theme_dir +'/src';
var scss_src_dir = src_dir +'/scss';
var js_src_dir = src_dir +'/js';
var lib_src_dir = src_dir +'/lib';

var scss_src = scss_src_dir +'/**/*.scss';
var scripts_src = js_src_dir +'/**/_*.js';
var compress_src = js_src_dir +'/**/[^_]*.js';
var ignore_src = js_src_dir +'/**/!*.js';

// ---------------------------------------------- Requirements

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    concat = require('gulp-concat'),
    minify = require('gulp-babel-minify'),
    rename = require('gulp-rename'),
    postcss = require('gulp-postcss'),
    autoprefixer = require('autoprefixer');

// ---------------------------------------------- Define Tasks

function style() {
    console.log('Processing styles...');
    return gulp.src(scss_src)
        .pipe(sourcemaps.init())
        .pipe(sass({
            includePaths: [
                'node_modules/bootstrap/scss/'
            ],
            outputStyle: 'compressed'
        }).on('error', sass.logError))
        .pipe(postcss([ autoprefixer ]))
        .pipe(sourcemaps.write(maps_dir))
        .pipe(gulp.dest(css_dir));
}

function scripts() {
    console.log('Processing scripts...');
    return gulp.src([
            lib_src_dir +'/modernizr/modernizr.js',
            lib_src_dir +'/gsap/**/*.js',
            scripts_src,
        ])
        .pipe(sourcemaps.init())
        .pipe(concat('scripts.min.js'))
       .pipe(minify({
            mangle: {
                keepClassName: true
            }
        }))
        .pipe(sourcemaps.write(maps_dir))
        .pipe(gulp.dest(js_dir));
}

function compress() {
    console.log('Compressing scripts...');
    return gulp.src([compress_src,'!'+ignore_src])
       .pipe(minify({
            mangle: {
                keepClassName: true
            }
        }))
        .pipe(rename({ suffix:'.min' }))
        .pipe(gulp.dest(js_dir));
}

function watch() {
    console.log('Watching...');
    gulp.watch(scss_src, style);
    gulp.watch(scripts_src, scripts);
    gulp.watch([compress_src,'!'+ignore_src], compress);
}

function move(done) {
    console.log('Moving npm assets...');
    // fontawesome
    gulp.src('node_modules/@fortawesome/fontawesome-pro/webfonts/**.*')
    .pipe(gulp.dest(lib_dir +'/fontawesome/webfonts'));
    gulp.src('node_modules/@fortawesome/fontawesome-pro/css/fontawesome.min.css')
    .pipe(gulp.dest(assets_css_dir));
    // gsap
    gulp.src('node_modules/gsap/dist/gsap.min.js')
    .pipe(gulp.dest(lib_src_dir +'/gsap'));
    gulp.src('node_modules/gsap/dist/ScrollTrigger.min.js')
    .pipe(gulp.dest(lib_src_dir +'/gsap'));
    gulp.src('node_modules/gsap/dist/DrawSVGPlugin.min.js')
    .pipe(gulp.dest(lib_src_dir +'/gsap'));
    done();
}

// --------------------------------------- Export Tasks

exports.style = style;
exports.scripts = scripts;
exports.compress = compress;
exports.watch = watch;
exports.move = move;
exports.default = gulp.parallel(style,scripts,compress,watch);
