(function( $ ) {

    // get project

    function get_project( project_id ) {
        
        $.modal.open();

        $.ajax({
            url: labrys_ajax.ajax_url,
            type: 'post',
            data: {
                action: 'get_project',
                key: labrys_ajax.ajax_key,
                project: project_id
            },
            dataType: 'json',
            success: function( response ) {
                if ( response.id ) {
                    $.modal.display_content( response );
                }
            }
        });
    
    }
    
})(jQuery);