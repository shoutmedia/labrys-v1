(function( $ ) {

    // initialize map

    function initMap( $el ) {

        var customStyles = [
            {
                "stylers": [
                    {
                        "saturation": -100
                    },
                    {
                        "gamma": 1
                    }
                ]
            },
            {
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "poi.business",
                "elementType": "labels.text",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "poi.business",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "poi.place_of_worship",
                "elementType": "labels.text",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "poi.place_of_worship",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry",
                "stylers": [
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "water",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "saturation": 50
                    },
                    {
                        "gamma": 0
                    },
                    {
                        "hue": "#50a5d1"
                    }
                ]
            },
            {
                "featureType": "administrative.neighborhood",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#333333"
                    }
                ]
            },
            {
                "featureType": "road.local",
                "elementType": "labels.text",
                "stylers": [
                    {
                        "weight": 0.5
                    },
                    {
                        "color": "#333333"
                    }
                ]
            },
            {
                "featureType": "transit.station",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "gamma": 1
                    },
                    {
                        "saturation": 50
                    }
                ]
            }
        ];
    
        // Create map
        var mapArgs = {
            zoom        : $el.data('zoom') || 11,
            mapTypeId   : google.maps.MapTypeId.ROADMAP,
            // disableDefaultUI: true, // remove controls
            zoomControl: true,
            mapTypeControl: false,
            scaleControl: false,
            streetViewControl: false,
            rotateControl: false,
            fullscreenControl: false,
            styles: customStyles
        };

        var map = new google.maps.Map( $el[0], mapArgs );
    
        // Find marker elements within map
        var $markers = $el.find('.marker');
        // Add markers
        map.markers = [];
        $markers.each(function(){
            initMarker( $(this), map );
        });
    
        // Center map based on markers
        centerMap( map );
    
        // Return map instance
        return map;
    }

    // initialize marker
    
    function initMarker( $marker, map ) {
    
        // Get position from marker.
        var lat = $marker.data('lat');
        var lng = $marker.data('lng');
        var latLng = {
            lat: parseFloat( lat ),
            lng: parseFloat( lng )
        };

        var icon = $marker.data('icon');
        var link = $marker.data('link');
        var tooltip = $marker.data('tooltip');
    
        // Create marker instance

        if ( icon ) { // custom icon

            var marker_icon = {
                url: icon,
                scaledSize: new google.maps.Size(20, 20),
            };
            var marker = new google.maps.Marker({
                position : latLng,
                map: map,
                icon: marker_icon
            });

        } else { // default marker

            var marker = new google.maps.Marker({
                position : latLng,
                map: map
            });

        }
        
        // Append to reference for later use
        map.markers.push( marker );

        // If marker contains HTML, add it to an infoWindow
        if ( $marker.html() ) {
    
            // Create info window
            $.infowindow = new google.maps.InfoWindow({
                content: $marker.html()
            });
    
            // Show info window when marker is clicked
            google.maps.event.addListener(marker, 'click', function() {
                $.infowindow.open( map, marker );
            });

        } else if (link) {

            google.maps.event.addListener(marker, 'click', function() {
                window.location.href(link);
            });

            if ( tooltip ) {

                google.maps.event.addListener(marker, 'mouseover', function() {
                    $.infowindow = new google.maps.InfoWindow({
                        content: tooltip,
                        maxWidth: 320
                    });
                    $.infowindow.open( map, marker );
                });

                google.maps.event.addListener(marker, 'mouseout', function() {
                    $.infowindow.close();
                });

            }
        }
    }

    // center map

    function centerMap( map ) {
    
        // Create map boundaries from all map markers
        var bounds = new google.maps.LatLngBounds();
        map.markers.forEach(function( marker ){
            bounds.extend({
                lat: marker.position.lat(),
                lng: marker.position.lng()
            });
        });
    
        // Case: Single marker
        if ( map.markers.length == 1 ){
            map.setCenter( bounds.getCenter() );
    
        // Case: Multiple markers
        } else {
            map.fitBounds( bounds );
        }
    }
    
    // Render maps on page load

    $(function() {
        $('.acf-map-container').each(function() {
            var map = initMap( $(this) );
        });
        // legend
        $('.legend-bar').on('click', function(e) {
            e.preventDefault();
            if ($('.map-legend').hasClass('legend-open')) {
                $('.map-legend').removeClass('legend-open');
            } else {
                $('.map-legend').addClass('legend-open');
            }
        });
    });
    
})(jQuery);