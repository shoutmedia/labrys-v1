gsap.registerPlugin(DrawSVGPlugin);
gsap.registerPlugin(ScrollTrigger);

document.addEventListener("DOMContentLoaded",function() {

    if ( document.body.classList.contains('home') ) {
        // add home page animation
    }

    // animate

    var animated_items = document.querySelectorAll('.animate');

    animated_items.forEach(animated_item => {

        gsap.set(animated_item, { transformOrigin: "center center" });

        let tl = gsap.timeline({
            scrollTrigger: {
                trigger: animated_item,
                start: "top 75%"
            }
        });

        tl.addLabel("start");
        tl.fromTo(animated_item, { scale: 0.75, opacity: 0 }, { scale: 1, opacity: 1, duration: 1, ease: Power4.easeOut }, "start");

    });

    // icons

    var drawn_items = document.querySelectorAll('.draw');

    drawn_items.forEach(drawn_item => {
        drawn_item.style.visibility = 'hidden';
    });

    setTimeout(function () { // delay for img -> svg replacement

        var drawn_svgs = document.querySelectorAll('.draw svg');

        drawn_svgs.forEach(drawn_svg => {

            let tl = gsap.timeline({
                scrollTrigger: {
                    trigger: drawn_svg,
                    start: "bottom 100%"
                }
            });

            let drawn_paths = drawn_svg.querySelectorAll('path, rect, circle, ellipse, polygon, polyline, line');

            tl.addLabel("start");
            tl.fromTo(drawn_paths, { drawSVG: "100% 100%" }, { drawSVG: "0% 100%", duration: 2, ease: Power4.easeOut }, "start");

        });

        drawn_items.forEach(drawn_item => {
            drawn_item.style.visibility = 'visible';
        });

    }, 400);

});