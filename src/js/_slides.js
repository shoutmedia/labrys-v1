(function($) {

    function parallax_slides() {
        "use strict";
         var speed = 0.25;
 
         var scrollPosition = $(window).scrollTop();
         var windowHeight = $(window).height();
 
         var slideCount = 0;
 
         $('.parallax-slide').each(function() {
             slideCount++;
             var parallaxPosition = 0;
             var backgroundHeight = 0;
 
             parallaxPosition = $(this).offset().top;
             backgroundHeight = $(this).outerHeight() + (windowHeight * speed); //* (1 + speed)
 
             if (scrollPosition + windowHeight > parallaxPosition && scrollPosition < parallaxPosition + $(this).closest('.slide').outerHeight()) {
                 var scrollOffset = Math.round((scrollPosition - parallaxPosition) * speed);
             }
 
             $(this).children('.image').css({
                 'transform':'translate3d(0,'+ scrollOffset +'px,0)',
                 'height': backgroundHeight +'px'
             });
         });
     }

    function slides_min_heights() {
        var windowWidth = $(window).width();
        // .slide-text
        $('.slides').each(function() {
            var minHeight = 0;
            $('.slide-text',this).css({'minHeight' : 'auto'});
            $('.slide-text', this).each(function() {
                var thisHeight = $(this).outerHeight();
                if (thisHeight > minHeight) {
                    minHeight = thisHeight;
                }
            });
            $('.slide-text',this).css({'minHeight' : minHeight +'px'});
        });
        // .image-container
        $('.slides').each(function() {
            var minHeight = 0;
            $('.image-container',this).css({'minHeight' : 'auto'});
            $('.image-container', this).each(function() {
                var thisHeight = $(this).outerHeight();
                if (thisHeight > minHeight) {
                    minHeight = thisHeight;
                }
            });
            $('.image-container',this).css({'minHeight' : minHeight +'px'});
            if (windowWidth < 1200) {
                var minHeight = 0;
                $('.slide-container', this).each(function() {
                    var thisHeight = $(this).outerHeight();
                    if (thisHeight > minHeight) {
                        minHeight = thisHeight;
                    }
                });
                $('.image-container.no-text',this).css({'minHeight' : minHeight +'px'});
            }
        });
    }

    $(window).on('load', function() {
        $('.slides').each(function() {
            var slideCount = $('.slide',this).length;
            var showDots = (slideCount > 1) ? true : false;
            $(this).slick({
                arrows: false,
                dots: showDots,
                autoplay: true,
                autoplaySpeed: 8000,
                fade: true,
                draggable: true,
                touchMove: true,
                infinite: true,
                customPaging : function(slider, i) {
                    var j = i + 1;
                    return '<a><span class="dot-text">Slide '+ j +'</span></a><span class="spacer"></span>';
                },
            });
        });
        $('.slick-dots').wrap('<div class="container"><div class="row"><div class="col"></div></div></div>');
    });

    $(window).on('resize', function() {
        slides_min_heights();
        parallax_slides();
    });

    $(document).on('scroll', function() {
        parallax_slides();
    });

})( jQuery );
