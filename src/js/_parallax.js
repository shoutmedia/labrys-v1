(function($) {

    function parallax_image() {
       "use strict";
        var speed = 0.25;

        var scrollPosition = $(window).scrollTop();
        var windowHeight = $(window).height();

        $('.parallax').each(function() {
            var backgroundHeight = 0;
            var scrollOffset = 0;
            var parallaxTop = 0;
            
            var thisHeight = $(this).outerHeight();
            var thisPosition = $(this).offset().top;
            backgroundHeight = Math.ceil( thisHeight + (windowHeight * speed) );

            var scrollOffset = 0;
            if (scrollPosition + windowHeight > thisPosition && scrollPosition < thisPosition + thisHeight) {
                var scrollOffset = Math.round((scrollPosition - thisPosition) * speed);
            }

            if ($(this).closest('.banner').length) {
                var parallaxTopOffset = 50; // percent
                var topScrollOffset = Math.round(thisPosition * speed);
                parallaxTop = Math.floor( (backgroundHeight - thisHeight - topScrollOffset) * (parallaxTopOffset / 100) * -1 );
            }

            $(this).children('img').css({
                'transform' : 'translate3d(0,'+ scrollOffset +'px,0)',
                'height' : backgroundHeight +'px',
                'top' : parallaxTop +'px'
            });
        });
    }

    function parallax_background() {
        "use strict";
        var speed = 0.25;
        var scrollPosition = $(window).scrollTop();
        var windowHeight = $(window).height();
        $( '.parallax-background' ).each(function() {
            var parallaxPosition = 0;
            var scrollOffset = 0;
            parallaxPosition = $(this).offset().top;
            if (scrollPosition + windowHeight > parallaxPosition && scrollPosition < parallaxPosition + $(this).outerHeight()) {
                var scrollOffset = Math.round((scrollPosition - parallaxPosition) * speed);
            }
            var bgPosX = '50%';
            var bgPos = $(this).css('backgroundPosition');
            if (bgPos) {
                var bgPosSplit = bgPos.split(' ');
                var bgPosX = bgPosSplit[0];
            }
            // set position
            $(this).css({
                'transform' : 'translate3d(0,0,0)',
                'background-position' : bgPosX +' '+ scrollOffset +'px'
            });
            // fit height
            if ( $(this).hasClass( 'parallax-fit-height' ) ) {
                var backgroundHeight = $(this).outerHeight() + (windowHeight * speed);
                $(this).css( 'background-size', 'auto '+ backgroundHeight +'px' );
            }
        });
    }

    $(window).on('load', function(){
        parallax_image();
        parallax_background();
    });

    $(window).on('resize', function(){
        parallax_image();
        parallax_background();
    });

    $(document).on('scroll', function(){
        parallax_image();
        parallax_background();
    });


})( jQuery );
