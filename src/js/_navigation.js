(function($) {

    function menu_toggle() {
        if ($('body').hasClass('nav-open')) {
            $('body').removeClass('nav-open');
            $('body').addClass('nav-closed');
        } else {
            $('body').removeClass('nav-closed');
            $('body').addClass('nav-open');
        }
    }

    $(document).on('click', '.menu-toggle, .nav-open #container', function(e) {
        e.preventDefault();
        menu_toggle();
    });

    $('.nav-open #container').on('click', function(e) {
        e.preventDefault();
        if ($('body').hasClass('nav-open')) {
            $('body').removeClass('nav-open');
            $('body').addClass('nav-closed');
        } else {
            $('body').removeClass('nav-closed');
            $('body').addClass('nav-open');
        }
    });

    function two_column_menu() {
        var nav_selector = '.main-nav .menu-container';
        var li_count = 6;
        $(nav_selector).find('ul.sub-menu').each( function () {
            if ( $(this).find('> li').length >= li_count ) {
                $(this).addClass( 'two-column' );
            }
        });
    }

    two_column_menu();

    function accordion_menu_bind() {
        var nav_selector = '.mobile-nav .menu-container';
        if ( !$(nav_selector).hasClass('accordion-menu') ) {
            $(nav_selector).addClass('accordion-menu');
            // hide submenus
            $(nav_selector).find('ul.sub-menu').hide();
            // open current children
            $(nav_selector).find('li.current-menu-item').children().show();
            // open current parents
            $(nav_selector).find('li.current-menu-item').parents().show();
            // add open class to parent
            $(nav_selector).find('li.current-menu-item').closest('li.menu-item-has-children').addClass('accordion-menu-open');
            // add click event
            $(nav_selector).find('li.menu-item-has-children').on('click', function(e) {
                if ( e.target.nodeName.toLowerCase() == 'a' ) {
                    var link = $( e.target ).attr('href');
                    window.location.href = link;
                    return false;
                }
                var ul = $(this).find('> ul.sub-menu');
                if ( ul.length ) {
                    if ( ul.is(':visible') ) {
                        ul.slideUp(400, function() {
                            $(this).closest('li').removeClass('accordion-menu-open');
                        });
                    }
                    if ( !ul.is(':visible') ) {
                        $(nav_selector).find('li').removeClass('accordion-menu-open');
                        $(nav_selector).find('ul.sub-menu').slideUp(400);
                        ul.slideDown(400, function() {
                            $(this).closest('li').addClass('accordion-menu-open');
                        });
                        return false;
                    }
                }
            });
        }
    }
    
    function accordion_menu_unbind() {
        var nav_selector = '.mobile-nav .menu-container';
        if ( $(nav_selector).hasClass('accordion-menu') ) {
            $(nav_selector).removeClass('accordion-menu');
            $(nav_selector).find('.accordion-menu-open').removeClass('accordion-menu-open');
            $(nav_selector).find('li.menu-item-has-children').off('click'); 
            $(nav_selector).find('ul.sub-menu').show();
            $(nav_selector).find('ul.sub-menu ul.sub-menu').hide();
        }
    }

    function menu_check() {
        var winWidth = $(window).width();
        if (winWidth >= 992) {
            $('body').removeClass('nav-open');
            $('body').removeClass('nav-closed');
            accordion_menu_unbind();
        } else {
            accordion_menu_bind();
        }
    }

    $( window ).on('resize', function() {
        menu_check();
    });

    menu_check();

})( jQuery );
