(function( $ ) {

    $.modal = {};
    
    $.modal.display_content = function( project ) {
        if ( $('#modal').length ) {
            $('.modal-content').html( project.content );
            $('.modal-close').on('click', function(e) {
                e.preventDefault();
                $.modal.close();
            });
            setTimeout(function() {
                $('.modal-content').addClass('loaded');
            }, 1);
        }
    };

    $.modal.open = function() {
        if ( !$('#modal').length ) {
            var modal = '<div id="modal" role="dialog" aria-modal="true">';
                modal += '<div class="modal-overlay"></div>';
                modal += '<div class="modal-wrapper">';
                    modal += '<div class="modal-content"></div>';
                modal += '</div>';
            modal += '</div>';
            $('body').append(modal);
            $('body').addClass('modal');
            setTimeout(function() {
                $('#modal').addClass('active');
            }, 1);
            $(document).on('click', function(e) {
                if ( $('#modal').hasClass('active') ) {
                    if ($(e.target).hasClass('modal-overlay')) {
                        $.modal.close();
                    }
                }
            });
        }
    };

    $.modal.close = function() {
        if ($('#modal').length) {
            $('#modal').removeClass('active');
            $('.modal-content').removeClass('loaded');
            setTimeout(function() {
                $('body').removeClass('modal');
                $('#modal').remove();
            }, 400);
        }
    };

})(jQuery);