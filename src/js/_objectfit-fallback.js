(function($) {

    if ( ! Modernizr.objectfit ) {
        $('.object-fit').each(function () {
            var img_src = $(this).find('img').prop('src');
            if (img_src) {
                $(this).css('backgroundImage', 'url(' + img_src + ')').addClass('no-object-fit');
            }
        });
    }

})( jQuery );
