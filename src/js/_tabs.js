(function($) {
    var tabs = $('[role="tab"]');
    var tabFocus = 0;

    $(tabs).each(function() {
        $(this).on('click', function(e) {
            changeTabs(this);
        });
    });

    $(document).on('keydown', function(e) {
        if ( $.inArray( e.which, [ 37, 38, 39, 40 ] ) >= 0 ) {
            e.preventDefault();
            switch (e.which) {
                case 37: // left arrow key
                case 38: // up arrow key
                    tabFocus--;
                    if ( tabFocus < 0 ) {
                        tabFocus = tabs.length - 1;
                    }
                break;
                case 39: // right arrow key
                case 40: // bottom arrow key
                    tabFocus++;
                    if ( tabFocus >= tabs.length ) {
                        tabFocus = 0;
                    }
                break;
            }
            $(tabs).eq(tabFocus).attr('tabindex', 0);
            $(tabs).eq(tabFocus).focus();
        }
    });

    function changeTabs(target) {
        var parent = $(target).closest('[role="tablist"]');
        var grandparent = $(parent).closest('.acf-tabs');
        var panel_selector = '#'+ $(target).attr('aria-controls');
        // deselect selected
        $(parent).find('[role="tab"]').attr('aria-selected', false);
        $(parent).find('[role="tab"]').attr('tabindex', -1);
        $(parent).find('[role="tab"]').removeClass('selected');
        // select target
        $(target).attr('aria-selected', true);
        $(target).attr('tabindex', 0);
        $(target).addClass('selected');
        // hide panels
        $(grandparent).find('[role="tabpanel"]').attr('hidden', true);
        // show selected panel
        $(panel_selector).attr('hidden', false);
    }

    // initialize
    // changeTabs( $(tabs).eq(tabFocus) );

})( jQuery );