(function($) {

    $("[data-object-position]").each(function() { // img
        var object_position = $(this).data('objectPosition');
        $('img', this).css({ 'object-position': object_position });
    });

})( jQuery );
