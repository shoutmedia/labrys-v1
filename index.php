<?php
	get_header();

	$queried_object = get_queried_object();
	$title = ( is_object( $queried_object ) && isset( $queried_object->name ) ) ? $queried_object->name : get_bloginfo('name');
	labrys_render_banner( '<h1>'. $title .'</h1>' );
?>
<main id="main">
	<div class="container">
		<div class="row">
			<div class="col">
				<?php
					$posts = get_posts();
					echo labrys_get_post_feed( $posts );
				?>
			</div>
		</div>
	</div>
</main>

<?php
    get_footer();
