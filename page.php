<?php get_header(); ?>

<?php labrys_render_banner(); ?>
<main id="main">
	<div class="container">
		<div class="row">
			<div class="col">
                <?php
                    if ( have_posts() ) {
                        while ( have_posts() ) {
                            the_post();
                            the_content();
                        }
                    }
                ?>
            </div>
		</div>
	</div>
</main>

<?php
    get_footer();