<?php

// theme domain
define('DOMAIN', 'labrys');

// labrys_get_colors

function labrys_color_palette() {
    $color_palette = [
        [
            'name'  => 'Color 1',
        	'slug'  => 'color-1',
        	'color'	=> '#344F59',
        ],
		[
            'name'  => 'Color 2',
        	'slug'  => 'color-2',
        	'color'	=> '#597D7D',
        ],
		[
            'name'  => 'Color 3',
        	'slug'  => 'color-3',
        	'color'	=> '#B06136',
		],
		[
			'name'  => 'Black',
			'slug'  => 'black',
			'color'	=> '#000000',
		],
		[
			'name'  => 'Dark Grey',
			'slug'  => 'grey-dark',
			'color'	=> '#333333',
		],
		[
			'name'  => 'Grey',
			'slug'  => 'grey',
			'color'	=> '#757575',
		],
		[
			'name'  => 'Light Grey',
			'slug'  => 'grey-light',
			'color'	=> '#dddddd',
		],
		[
			'name'  => 'White',
			'slug'  => 'white',
			'color'	=> '#ffffff',
		]
	];
    return $color_palette;
}

// labrys_setup

if ( !function_exists('labrys_setup') ) {
	function labrys_setup() {

		register_nav_menus( array(
			'main' => __( 'Main Menu', DOMAIN ),
			'footer' => __( 'Footer Menu', DOMAIN )
		) );

		add_theme_support( 'title-tag' );
		add_post_type_support( 'page', 'excerpt' );
		add_theme_support( 'responsive-embeds' );

		// thumbnails
		add_theme_support( 'post-thumbnails' );
		add_image_size( 'extra_large', 1920 );

		// custom logo option
		add_theme_support( 'custom-logo' ); // custom logo

		// editor styles
		add_theme_support( 'editor-styles' );
		add_theme_support( 'align-wide' );
		add_theme_support( 'editor-font-sizes', array() );
		add_theme_support( 'disable-custom-font-sizes' );
		add_theme_support( 'disable-custom-colors' );
		add_theme_support( 'editor-color-palette', labrys_color_palette() );
		add_editor_style( get_stylesheet_directory_uri() .'/assets/css/wysiwyg.css?v='. filemtime( get_stylesheet_directory() .'/assets/css/wysiwyg.css' ) );
		add_editor_style( get_stylesheet_directory_uri() .'/assets/css/fontawesome.min.css?v='. filemtime( get_stylesheet_directory() .'/assets/css/fontawesome.min.css' ) );

		/*
		// woocommerce
		add_theme_support( 'woocommerce' );
		add_theme_support( 'wc-product-gallery-zoom' );
		add_theme_support( 'wc-product-gallery-lightbox' );
		add_theme_support( 'wc-product-gallery-slider' );
		*/

	}
}
add_action('after_setup_theme','labrys_setup');

// labrys_thumbnail_sizes

if ( !function_exists('labrys_thumbnail_sizes') ) {
	function labrys_thumbnail_sizes() {
        // thumbnail
        update_option( 'thumbnail_size_w', 320 );
        update_option( 'thumbnail_size_h', 320 );
        update_option( 'thumbnail_crop', 1 );
        // small
        update_option( 'small_size_w', 640 );
        update_option( 'small_size_h', 0 );
        // medium
        update_option( 'medium_size_w', 960 );
        update_option( 'medium_size_h', 0 );
        // medium_large
        update_option( 'medium_large_size_w', 1280 );
        update_option( 'medium_large_size_h', 0 );
        // large
        update_option( 'large_size_w', 1600 );
		update_option( 'large_size_h', 0 );
	}
}
add_action('switch_theme', 'labrys_thumbnail_sizes');

// ajax key

function labrys_ajax_key() {
    return 'labrys_ajax_key_value';
}

// enqueue scripts and styles

function labrys_scripts() {
	wp_enqueue_style( DOMAIN .'-style', get_stylesheet_uri(), array(), filemtime( get_stylesheet_directory() .'/style.css' ) );
	wp_enqueue_script( DOMAIN .'-script', get_template_directory_uri() .'/assets/js/scripts.min.js', array('jquery'), filemtime( get_stylesheet_directory() .'/assets/js/scripts.min.js' ) );
	wp_enqueue_script( DOMAIN .'-maps-script', 'https://maps.googleapis.com/maps/api/js?v=3.exp&key='. labrys_google_api_key() );
	wp_localize_script( DOMAIN .'-script', 'labrys_ajax',
        array(
            'ajax_url' => admin_url('admin-ajax.php'),
            'ajax_key' => wp_create_nonce( labrys_ajax_key() ),
            'img_path' => get_template_directory_uri() .'/assets/img/'
        )
    );
}
add_action( 'get_footer', 'labrys_scripts' );

// block editor scripts

function labrys_editor_scripts() {
	wp_enqueue_style( 'editor-style', get_stylesheet_directory_uri() .'/editor.css', array(), filemtime( get_stylesheet_directory() .'/editor.css' ), 'all' );
	wp_enqueue_style( 'editor-fontawesome', get_stylesheet_directory_uri() .'/assets/css/fontawesome.min.css', array(), filemtime( get_stylesheet_directory() .'/assets/css/fontawesome.min.css' ), 'all' );
	wp_enqueue_script( 'editor-scripts', get_stylesheet_directory_uri() .'/assets/js/admin/editor.js', array( 'wp-blocks', 'wp-dom' ), filemtime( get_stylesheet_directory() .'/assets/js/admin/editor.js' ), true );
}
add_action( 'enqueue_block_editor_assets', 'labrys_editor_scripts' );

// plugin check

function labrys_theme_dependencies() {
	if ( !function_exists('get_field') ) {
		$str = '<div style="display:flex; flex-direction:column; justify-content:center; align-items:center; width:100vw; height:100vh;">';
			$str .= '<p style="font-family:sans-serif;">'. __( 'This theme requires Advanced Custom Fields Pro to be installed and active.', DOMAIN ) .'</p>';
			$str .= '<p style="font-family:sans-serif;"><a href="'. admin_url( 'plugins.php' ) .'">'. __( 'Installed Plugins', DOMAIN ) .'</a></p>';
		$str .= '</div>';
		echo $str;
		exit;
	}
}
add_action( 'get_header', 'labrys_theme_dependencies' );

// custom mobile logo

function labrys_customize_register( $wp_customize ) {
	$wp_customize->add_setting( 'mobile_logo' );
	$wp_customize->add_control(
       new WP_Customize_Image_Control(
           $wp_customize,
           'mobile_logo',
           array(
               'label'      => __( 'Mobile Logo', DOMAIN ),
               'section'    => 'title_tagline',
               'settings'   => 'mobile_logo',
           )
       )
   );
}
add_action( 'customize_register', 'labrys_customize_register' ); // custom logo

// Remove unused functionality

remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

// Change greeting

function labrys_greeting($wp_admin_bar) {
    $my_account = $wp_admin_bar->get_node('my-account');
    $title = str_replace('Howdy,', __('Hi', DOMAIN), $my_account->title );
    $wp_admin_bar->add_node( array(
        'id' => 'my-account',
        'title' => $title,
        )
    );
}
add_filter('admin_bar_menu', 'labrys_greeting', 12);

// change sender name

function labrys_sender_name( $original_email_from ) {
    return get_bloginfo('name');
}
add_filter( 'wp_mail_from_name', 'labrys_sender_name' );

// include reusable blocks in dashboard

function labrys_reusable_block_admin() {
    add_menu_page( 'Reusable Blocks', 'Reusable Blocks', 'edit_posts', 'edit.php?post_type=wp_block', '', 'dashicons-editor-table', 22 );
}
add_action( 'admin_menu', 'labrys_reusable_block_admin' );

// add search to menu

function labrys_nav_menu_items ( $items, $args ) {
    if ( $args->theme_location == 'main' ) {
		$items .= '<li class="search-link">'. get_search_form( array( 'echo' => false ) ) .'</li>';
    }
    return $items;
}
// add_filter( 'wp_nav_menu_items', 'labrys_nav_menu_items', 10, 2 );

// includes

require get_template_directory() .'/inc/helpers.php';
require get_template_directory() .'/inc/tracking.php';
require get_template_directory() .'/inc/acf.php';
require get_template_directory() .'/inc/acf-blocks.php';
require get_template_directory() .'/inc/block-templates.php';
require get_template_directory() .'/inc/login.php';
require get_template_directory() .'/inc/svg.php';
require get_template_directory() .'/inc/shortcodes.php';