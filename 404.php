<?php get_header(); ?>

<?php labrys_render_banner('<h1>Page Not Found</h1>'); ?>
<main id="main" class="search">
    <div class="container">
        <div class="row">
            <div class="col">
                <p>It looks like nothing was found at this location. Maybe try a search below?</p>
                <?php
					get_search_form();
				?>
            </div>
        </div>
    </div>
</main>

<?php
    get_footer();
