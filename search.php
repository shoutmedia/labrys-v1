<?php get_header(); ?>

<?php labrys_render_banner('<h1>Search</h1>'); ?>
<main id="main">
    <div class="container">
        <div class="row">
            <div class="col">
                <?php
                    $search_query = get_search_query();
                    echo '<h2>Search'. ( $search_query ? ' Results: <span>'. $search_query .'</span>' : '' ) .'</h2>';
                    echo get_search_form();
                    if ($_GET['s']) {
                        if ( have_posts() ) {
                                while ( have_posts() ) {
                                the_post();
                                echo '<article id="' . get_the_ID() . '">';
                                    the_title( sprintf( '<h3 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' );
                                    the_excerpt();
                                echo '</article>';
                                }
                                echo '<nav class="pagination">';
                                echo paginate_links( array(
                                'total' => $wp_query->max_num_pages,
                                'prev_text'          => __('Previous', DOMAIN),
                                'next_text'          => __('Next', DOMAIN),
                            ) );
                            echo '</nav>';
                        } else {
                            echo '<p>Sorry, but nothing matched your search terms. Please try again with some different keywords.</p>';
                        }
                    }
                ?>
            </div>
        </div>
    </div>
</main>

<?php
    get_footer();
