<?php get_header(); ?>

<?php labrys_render_banner(); ?>
<main id="main">
	<div class="container">
		<div class="row">
			<div class="col">
				<?php
					labrys_render_banner();
					$queried_object = get_queried_object();
					$title = ( is_object( $queried_object ) && isset( $queried_object->name ) ) ? $queried_object->name : get_bloginfo( 'name' );
					echo '<h1 class="has-text-align-center">'. $title .'</h1>';
					$posts = get_posts();
					echo labrys_get_post_feed( $posts );
				?>
			</div>
		</div>
	</div>
</main>

<?php
    get_footer();
