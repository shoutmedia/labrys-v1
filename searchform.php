<form role="search" method="get" class="searchform" action="<?php echo home_url( '/' ); ?>">
    <label for="s" class="screen-reader-text"><?php _e('Search for',DOMAIN); ?></label>
    <input type="search" id="s" name="s" value="<?php the_search_query(); ?>" placeholder="Search" />
    <button type="submit"><span class="fas fa-search"></span></button>
</form>